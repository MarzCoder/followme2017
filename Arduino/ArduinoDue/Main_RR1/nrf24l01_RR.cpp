#include "nrf24l01_RR.h"

RF24 radio(9,10);

byte adresses[][6] = {"1Boat"};

byte counter = 0;

char incomingMessage[32];

float target[7];

int32_t position[2];

float velocity[2];

float headingQuaternion[2];

float yawRate;

byte mode = 0;

int8_t steering_ref = 0;

uint8_t throttle_ref = 0;

byte** acknowledgeFIFO = new byte*[10];

byte** acknowledgeFIFOEnd = acknowledgeFIFO+9;

byte** getAckFIFOPtr = acknowledgeFIFO;

byte** putAckFIFOPtr = acknowledgeFIFO+1;

byte quatVec[32] = {'q','u','a','t','e','r','n','i','o','n'};

byte posVec[32] = {'p','o','s','i','t','i','o','n'};

byte errorVec[32] = {'e','r','r','o','r','V','e','c'};

//byte nullVec[32] = {0};

byte nullVec[32] = {'n','u','l','l'};

byte GPSData[21] = {'G'};

byte controlData[4] = {'C'};

byte compassCorrectionData[5] = {'M'};

byte  PID_parametersAV[16] = {0};
byte  PID_parametersA[16] = {0};
byte  PID_parametersT[16] = {0};
uint8_t  newPIDAV =0;
uint8_t  newPIDA=0;
uint8_t  newPIDT=0;

uint32_t timeSinceLastMode;

uint32_t timeSinceLastTarget;

void setupRadio(){
  radio.begin();
  Serial.begin(115200);
 // radio.setDataRate(RF24_250KBPS);
  radio.setPALevel(RF24_PA_MAX);

  radio.enableAckPayload();                     // Allow optional ack payloads
  radio.enableDynamicPayloads();                // Ack payloads are dynamic payloads

  radio.openReadingPipe(1,adresses[0]);
  radio.startListening();                       // Start listening  
  
  //radio.writeAckPayload(1,&counter,1);          // Pre-load an ack-paylod into the FIFO buffer for pipe 1

  setupAckFIFO();

  loadAck();
}

void setupAckFIFO(){
  for(int i=0;i<10;i++)
  {
    acknowledgeFIFO[i]=nullVec;
  }
}

//void readRadio(){
//  while(radio.available())
//  {
//    radio.read(incomingMessage,32);
//    for(int i=0;i<6;i++)
//    {
////      target[i]=*(int32_t*)(incomingMessage+1+i*4);
//      target[i]=((int32_t*)(incomingMessage+1))[i];
//      Serial.println(target[i]);
//    }
//    Serial.println("");
//    radio.writeAckPayload(1,incomingMessage,25);
//  }
//}

void receiveRadioMessages(){
//  Serial.println("herp");
  while( radio.available()){
//    Serial.println("Got message!");
    radio.read(incomingMessage,32);
    switch(incomingMessage[0]){
      case 't' :
//        Serial.println("target");
//        for(int i=0;i<7;i++)
//        {
//    //      target[i]=*(int32_t*)(incomingMessage+1+i*4);
//          target[i]=((float*)(incomingMessage+1))[i];
        
         // Serial.println(target[i]);
//        }
        timeSinceLastTarget = micros();
        position[0] = *(int32_t*)(incomingMessage+1);
        position[1] = *(int32_t*)(incomingMessage+5);
        velocity[0] = *(float*)(incomingMessage+9);
        velocity[1] = *(float*)(incomingMessage+13);
        headingQuaternion[0] = *(float*)(incomingMessage+17);
        headingQuaternion[1] = *(float*)(incomingMessage+21);
        yawRate = *(float*)(incomingMessage+25);
        //Serial.println("");
      break;
      case 'm' :
//      Serial.println("mode:");
        mode = incomingMessage[1];
        timeSinceLastMode = micros();
//      Serial.println(mode);
      break;
      case 'c' :
        steering_ref = (int8_t)incomingMessage[1];
        throttle_ref = (uint8_t)incomingMessage[2];
      break;
      case 'q' :
      //Serial.println("quaternion");
        putAckFIFO(quatVec);
      break;
      case 'p' :
      //Serial.println("position");
        putAckFIFO(posVec);
      break;
      case 'e' :
      //Serial.println("errorvec");
        putAckFIFO(errorVec);
      break;
      case 'G' :
      //Serial.println("GPSData");
        putAckFIFO(GPSData);
      break;
      case 'C' :
      //Serial.println("GPSData");
        putAckFIFO(controlData);
      break;
      case 'M' :
      //Serial.println("compassCorrection");
        putAckFIFO(compassCorrectionData);
      break;
      case 'V' :
      for(int i = 0;i<16;i++)
      {
        PID_parametersAV[i] = incomingMessage[i+1];
      }
      newPIDAV=1;

      break;
      case 'A' :
      for(int i = 0;i<16;i++)
      {
        PID_parametersA[i] = incomingMessage[i+1];
      }
      newPIDA=1;

      break;
      case 'T' :
      for(int i = 0;i<16;i++)
      {
        PID_parametersT[i] = incomingMessage[i+1];
      }
      newPIDT=1;

      break;
      default :
      break;
    }
    loadAck();
  }
}
int8_t getMode(){
  if(1000000>(micros()-timeSinceLastMode))
  {
    if((mode!=1 && mode!=3)||2000000>(micros()-timeSinceLastTarget))
    {
      return mode;
    }
    else
    {
      return 0;
    }
  }
  else
  {
    mode = 0;
    return mode;
  }
}
//float* getTarget(){
//  return target;
//}

int32_t* getPosition(){
  return position;
}
float* getVelocity(){
  return velocity;
}

float* getQuaternion(){
  return headingQuaternion;
}

float getYawrate(){
  return yawRate;
}
int8_t getSteeringRef(){
  return steering_ref;
}

uint8_t getThrottleRef(){
  return throttle_ref;
}

void setQuaternion(int32_t q[4]){
   int32_t* q1 = (int32_t*)(quatVec+1);
   q1[0]=q[0];
   q1[1]=q[1];
   q1[2]=q[2];
   q1[3]=q[3];
}
uint8_t   getNewPIDD(){
  if(newPIDAV==1)
  {
    newPIDAV=0;
    return 1;
  }
  else
    return 0;
}
uint8_t   getNewPIDA(){
  if(newPIDA==1)
  {
    newPIDA=0;
    return 1;
  }
  else
    return 0;
}
uint8_t   getNewPIDV(){
  if(newPIDT==1)
  {
    newPIDT=0;
    return 1;
  }
  else
    return 0;
}

float*  getPIDD(){
  return (float*)PID_parametersAV;
}
float*  getPIDA(){
  return (float*)PID_parametersA;
}
float*  getPIDV(){
  return (float*)PID_parametersT;
}

void setGPSData(int32_t lon, int32_t lat,int32_t velN,int32_t velW, uint32_t lastReceivedGPS){
  int32_t* GPSData1 = (int32_t*)(GPSData+1);
  *GPSData1 = lon;
  *(GPSData1+1) = lat;
  *(GPSData1+2) = velN;
  *(GPSData1+3) = velW;
  *(GPSData1+4) = (int32_t)lastReceivedGPS;
}

void setMagCorrection(float magCorrection){
  float* start = (float*)(compassCorrectionData+1);
  *start = magCorrection;
}

void setControlData(uint8_t mode, int8_t rudder, uint8_t throttle){
  controlData[1] = (byte) mode;
  controlData[2] = (byte) rudder;
  controlData[3] = (byte) throttle;
}
void putAckFIFO(byte* data){
  *putAckFIFOPtr = data;
  if(++putAckFIFOPtr>acknowledgeFIFOEnd)
  {
    putAckFIFOPtr = acknowledgeFIFO;
  }
}

byte* getAckFIFO(){
//  byte* ptr = *getAckFIFOPtr;
//  *getAckFIFOPtr = nullVec;
//  if(getAckFIFOPtr<putAckFIFOPtr)
//  {
//    if(++getAckFIFOPtr==putAckFIFOPtr)
//    {
//      getAckFIFOPtr--;
//    }
//  }
//  else
//  {
//    if(++getAckFIFOPtr>acknowledgeFIFOEnd)
//    {
//      getAckFIFOPtr=acknowledgeFIFO;
//      if(getAckFIFOPtr==putAckFIFOPtr)
//      {
//        getAckFIFOPtr = acknowledgeFIFOEnd;
//      }
//    }
//  }
//  return ptr;
  if(getAckFIFOPtr<putAckFIFOPtr)
  {
    if(++getAckFIFOPtr==putAckFIFOPtr)
    {
      getAckFIFOPtr--;
    }
  }
  else
  {
    if(++getAckFIFOPtr>acknowledgeFIFOEnd)
    {
      getAckFIFOPtr=acknowledgeFIFO;
      if(getAckFIFOPtr==putAckFIFOPtr)
      {
        getAckFIFOPtr = acknowledgeFIFOEnd;
      }
    }
  }
  byte* ptr = *getAckFIFOPtr;
  *getAckFIFOPtr = nullVec;
  return ptr;
}

void calcPIDcoeffs(){
  
}

void loadAck(){
  byte *ptr = getAckFIFO();
  if(ptr!=nullVec)
  {
    radio.writeAckPayload(1,ptr,32); 
  }
//  else
//  {
//    Serial.println("NULL");
//  }
}
