#include  "Guidance_support_functions.h"

void rotate2DByQuaternion(float result[2], float q0, float q3, float vector[2]){
  
//  float Q[2];
//  
//  Q[0] = sq(q0)-1;
//  Q[1] = -q0*q3;
//
//  float norm = norm2D(Q);
//  Q[0]=Q[0]/norm;
//  Q[1]=Q[1]/norm;
//
//  result[0] = Q[0]*vector[0] + Q[1]*vector[1];
//  result[1] = -Q[1]*vector[0] + Q[0]*vector[1];
    float Q[2];
    float norm = sqrt(sq(q0)+sq(q3));
    Q[0] = 2*sq(q0/norm)-1;
    Q[1] = -2*q0*q3/sq(norm);
  
//    float norm = norm2D(Q);
    Q[0]=Q[0];
    Q[1]=Q[1];
    
    result[0] = Q[0]*vector[0] + Q[1]*vector[1];
    result[1] = -Q[1]*vector[0] + Q[0]*vector[1];
    
}

void rotate2D(float result[2], float angle, float vector[2]){
  float R[2];
  R[0] = cos(angle);
  R[1] = -sin(angle);
  result[0] = R[0]*vector[0] + R[1]*vector[1];
  result[1] = -R[1]*vector[0] + R[0]*vector[1];
}

void rotateR(float result[2], float angle, float R1[2]){
  //R*R1
  float R[2];
  R[0] = cos(angle);
  R[1] = -sin(angle);
  result[0] = R[0]*R1[0] -  R[1]*R1[1];
  result[1] = R[0]*R1[1] + R[1]*R1[0];
}

void Diff2D(float result[4],float minuend[7], float subtrahend[7]){
  result[0] = minuend[0]-subtrahend[0];
  result[1] = minuend[1]-subtrahend[1];
  result[2] = minuend[2]-subtrahend[2];
  result[3] = minuend[3]-subtrahend[3];
}

void predictSystem(float xpred[4],float posDiff[2], float velDiff[2], float headingQuatenion[2],float yawRate, float lat2m, float lon2m, float stepSize){
  xpred[0] = posDiff[0]*lat2m + velDiff[0]*stepSize;
  xpred[1] = posDiff[1]*lon2m + velDiff[1]*stepSize;
  float qdot[2];
  qdot[0] = cos(yawRate*stepSize/2);
  qdot[1] = sin(yawRate*stepSize/2);
  xpred[2] = (headingQuatenion[0]*qdot[0] - headingQuatenion[1]*qdot[1])*stepSize;
  xpred[3] = (headingQuatenion[0]*qdot[1] + headingQuatenion[1]*qdot[0])*stepSize;
  
//  xpred[2] = positionx[4] + positionx[5]*stepSize;
}

float norm2D(float vec[2]){
  return sqrt(sq(vec[0])+sq(vec[1]));
}

void transformInputs(float* rudder, float* throttle, float x, float y){
  if(x<0.00001)
  {
    x=0.00001;
  }
//  else if(x>1)
//  {
//    x=1;
//  }
  *rudder = atan(y/x);
  if(*rudder>0.5236)
  {
    *rudder=0.5236;
  }
  else if(*rudder<-0.5236)
  {
    *rudder=-0.5236;
  }
  float temp1 = sin(*rudder);
  float temp2 = cos(*rudder);
  *throttle = 0.2*y/(temp1+0.001)+0.8*x/temp2;
  if(*throttle*temp2>x+0.2)
  {
    *throttle=(x+0.2)/temp2;
  }
//  if(*throttle>1)
//  {
//    *throttle=1;
//  }
}


