#include <SPI.h>
#include <RF24.h>



void setupRadio();

void setupAckFIFO();

void receiveRadioMessages();

int8_t getMode();

//float* getTarget();

int32_t* getPosition();

float* getVelocity();

float* getQuaternion();

float getYawrate();

int8_t getSteeringRef();

uint8_t getThrottleRef();

uint8_t   getNewPIDD();
uint8_t   getNewPIDA();
uint8_t   getNewPIDV();

float*  getPIDD();
float*  getPIDA();
float*  getPIDV();

void setGPSData(int32_t lon, int32_t lat,int32_t velN, int32_t velW, uint32_t lastReceivedGPS);

void setControlData(uint8_t mode, int8_t rudder, uint8_t throttle);

void setQuaternion(int32_t q[4]);

void setMagCorrection(float magCorrection);

void putAckFIFO(byte* data);

byte* getAckFIFO();

void loadAck();

void calcPIDcoeffs();



