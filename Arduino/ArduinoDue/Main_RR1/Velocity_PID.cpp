#include "Velocity_PID.h"
#include "arduino.h"
static float Kp = 0.7;
static float Ki = 0.0;
static float Kd = 0.0;
static float Tf = 0.26;
static float  h = 0.05;

static float a0;
static float a1;
static float a2;
static float b0;
static float b1;
static float b2;

static float output[3] = {0,0,0};
static float input[3] = {0,0,0};

void resetPID_Velocity(){
  input[2] = 0;
  input[1] = 0;
  input[0] = 0;
  output[2] = 0;
  output[1] = 0;
  output[0] = 0;
}

void calcPIDParameters_Velocity(){
  a0 = 4*Tf+2*h;
  b0 = ((2*Tf+h)*(2*Kp+h*Ki)+4*Kd)/a0;
  b1 = (-8*Kd-2*Kp*(2*Tf+h)+h*Ki*(2*Tf+h)-2*Tf*(2*Kp+h*Ki)+h*(2*Kp+h*Ki))/a0;
  b2 = (4*Tf*Kp-2*h*Tf*Ki-2*h*Kp+h*h*Ki+4*Kd)/a0;
  a1 = (-8*Tf)/a0;
  a2 = (4*Tf-2*h)/a0;
}

float updateOutput_Velocity(float newInput){
  input[2]=input[1];
  input[1]=input[0];
  input[0]=newInput;
  output[2]=output[1];
  output[1]=output[0];
  output[0] = input[0]*b0+input[1]*b1+input[2]*b2-output[1]*a1-output[2]*a2;
  return output[0];
}

float getLastOutput_Velocity(){
  return output[0];
}

void setPIDF_Velocity(float* parameters){
  Kp = parameters[0];
  Ki = parameters[1];
  Kd = parameters[2];
  Tf = parameters[3];
  Serial.println("Velocity PID updated, new values:");
  Serial.println(Kp);
  Serial.println(Ki);
  Serial.println(Kd);
  Serial.println(Tf);
  calcPIDParameters_Velocity(); 
  resetPID_Velocity();
}



