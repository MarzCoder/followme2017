#include <math.h>
#include <arduino.h>

void rotate2DByQuaternion(float result[2], float q0, float q3, float vector[2]);

void rotate2D(float result[2], float angle, float vector[2]);

void rotateR(float result[2], float angle, float R1[2]);

void Diff2D(float result[2],float minuend[4], float subtrahend[4]);

void predictSystem(float xpred[4],float posDiff[2], float velDiff[2], float headingQuatenion[2],float yawRate, float lat2m, float lon2m, float stepSize);

float norm2D(float vec[2]);

void transformInputs(float* rudder, float* throttle, float x, float y);

