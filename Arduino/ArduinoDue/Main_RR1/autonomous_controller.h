void updateControl(int32_t posLB[2], float velLB[2],float headingQuaternionLB[2],float yawRateLB,
int32_t posRR[2], float velRR[2],float headingQuaternionRR[2],float yawRateRR,float lat2m, float lon2m);
void applyControl();

void sendSteeringRef(int8_t steeringRef);
void sendThrottleRef(uint8_t throttleRef);
void sendMode(int8_t mode);
int8_t getRudder();
uint8_t getThrottle();

