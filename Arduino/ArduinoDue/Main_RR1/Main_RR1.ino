#include <Wire.h>
#include <SPI.h>
#include <stdio.h>
#include "MPU9250_definitions.h"
#include "HMC5983_definitions.h"
#include "QuaternionFunctionsInt.h"
#include "MatrixOperations.h"
#include <stdint.h>
#include "nrf24l01_RR.h"
#include <math.h>
#include "autonomous_controller.h"
#include "angle_PIDRR.h"
#include "Distance_PID.h"
#include "Velocity_PID.h"

// These definitions are used to simplify the setup of the "dataToSend"
// variable.
#define SEND_Q 0x1
#define SEND_GYR 0x2
#define SEND_ACC 0x4
#define SEND_MAG 0x8
#define SEND_GPS 0x10
#define SEND_DELAY 0x20
#define SEND_P 0x40
#define SEND_SETTINGS 0x80

// Used to simplify the setup of the "settings" variable.
#define RESET_Q 0x01
#define RESET_P 0x02
#define MAG_OFF 0x04

// Two 32 bit variables used to control what data is sent to MATLAB and
// other settings (currently only magnetometer update on/off).
int32_t dataToSend = 0;
int32_t settings = 0;

// Variable initialization. Note that these are stored as integers. This is because int operations
// are much faster than float operations on the arduino due. To represent values smaller than one 
// and to achive the highest possible precision each variable are assigned an assumed scale.
// For example the array q used to hold the unit quaternion (quaternion with length 1). It is initialized
// to [1 0 0 0] but to avoid round of errors it is scaled up by 2^15 and becomes [32768 0 0 0] instead.
// A power of 2 is used since the shift operators << and >> can be used to change scale efficiently. 
// See Variable scales.ods for each assumed scale.
int32_t q[4] = {32768,0,0,0}; // Array that holds the orientation estimate in quaternion format.
int32_t q0[4] = {32768,0,0,0}; // Initial orientation guess.
// Covariance matrix belonging to the quaternion.
int64_t P[4][4] = {{70368744,0,0,0},{0,70368744,0,0},{0,0,70368744,0},{0,0,0,70368744}};
//Initial values.
int64_t P0[4][4] = {{70368744,0,0,0},{0,70368744,0,0},{0,0,70368744,0},{0,0,0,70368744}};

int32_t gyr[3] = {0,0,0}; // Array that holds the latest gyroscope measurements (xyz).
// Covariance matrix for the gyroscope measurements
int64_t Rw[3][3] = {{3456442750,-180964530,104260},{-180964530,2678926230,-28452880},{104260,-28452880,2083390630}};

int32_t acc[3]; // Array that holds the latest accelerometer measurements (xyz).
// Covariance matrix for the accelerometer measurements
int64_t Ra[3][3]={{21805796051,494807613,431278077},{494807613,19206138668,2577081565},{431278077,2577081565,57967695683}};
int32_t g0[3]={0,0,10056}; // The nominal gravity vector
// Accelerometer bias
int32_t accBias[3] = {115343, 188744, -193987};

int32_t mag[3]; // Array that holds the latest magnetometer measurements (xyz).
// Covariance matrix for the magnetometer measurements
int64_t Rm[3][3]={{6871948,0,0},{0,6871948,0},{0,0,6871948}};
int32_t m0[3]={256,0,0}; // The nominal horizontal component of the Earth's magnetic field.
// Bias and scale error of the magnetometer
int32_t magBias[3]={200,-85,-153}; //scale 2^0
int32_t magScale[3]={31578,32216,34668}; //scale 2^15

// The step size of the filter 10486 corresponds to 0.0050001 seconds
int32_t stepSize = 10486;


//These variables are used to temporarily store the position and orientation data before its sent to the controller.
//They should probably not be global.
int32_t posRR[2];
float velRR[2];
float headingQuaternionRR[2];
float yawRateRR;



////======================================================
//// Messages used to send raw sensor data to MATLAB ®
////======================================================
byte accMessage[]={0x61,0x62,0x63,0x64,0x65,0x66,0x68,'\r','\n'}; // "a"...
short int* accData = (short int*)(accMessage+1);


byte gyrMessage[]={0x67,0x62,0x63,0x64,0x65,0x66,0x68,'\r','\n'}; // "g"...
short int* gyrData = (short int*)(gyrMessage+1);

byte magMessage[]={0x6D,0x62,0x63,0x64,0x65,0x66,0x68,'\r','\n'}; // "m"...
short int* magData = (short int*)(magMessage+1);

//======================================================
// Rate control variables initialization
//======================================================

short int gyroscope_divider = 0;
short int accelerometer_divider = 0;
short int magnetometer_divider = 0;
short int gps_divider = 0;
short int dataIn_divider = 0;
short int dataOut_divider = 0;
short int radio_divider = 0;
short int control_divider = 0;
unsigned short int Delay;
unsigned long timeMicro;

//======================================================
// GPS UBX NAV-PVT message format and handling
//======================================================
// The GPS receiver outputs its data over a serial bus.
// Only the NAV-PVT message is activated,
// see u-blox8-M8_ReceiverDescrProtSpec_(UBX-13003221)_Public.pdf

byte navpvt[98]; // The message is 98 bytes long

// A number of pointers are defined for easier access of the data.
uint32_t* iTOW = (uint32_t*)(navpvt+4);
uint8_t* flags = (uint8_t*)(navpvt+25);
int32_t* lon = (int32_t*)(navpvt+28);  // offset by an additional 4 bytes 
int32_t* lat = (int32_t*)(navpvt+32);  // for the class, id and 2xlength bytes.
uint32_t* hAcc = (uint32_t*)(navpvt+44);
uint32_t* vAcc = (uint32_t*)(navpvt+48);
int32_t* velN = (int32_t*) (navpvt+52);
int32_t* velE = (int32_t*) (navpvt+56);
int32_t* headMot = (int32_t*) (navpvt+68);
uint8_t* CK_A = (uint8_t*)(navpvt+96);
uint8_t* CK_B = (uint8_t*)(navpvt+97);

// A temporary storage for the message is used when it is being read.
// When a complete message has arrived the data is copied all at
// once to the navpvt array.
byte navpvt_temp[98];
byte* currentByteGPSMsg = navpvt_temp;
byte* endOfGPSMsg = navpvt_temp+98;

int foundGPSMsg = 0; // 1 indicates that the start of a new NAV-PVT message
// has been found. Reset to 0 when the end is reached.
uint32_t GPSReadStartTime = 0; // Time at which the start of the message
// was read. If the rest of the message does not arrive within a certain 
// time limit the message is assumed to be lost and the code start to look
// for the next message.
uint8_t newGPS = 0; // Set to 1 when a new complete message has arrived.
uint32_t GPSFixTimer = 0; // Stores the time of the last NAV-PVT message
// with a high fix ok flag.
uint32_t lastReceivedGPS = 0; // Time of last received message.


//======================================================
// I2C helping functions
//======================================================

void i2c_write(int address, byte reg, byte data) 
{
    // Send output register address
    Wire.beginTransmission(address);
    Wire.write(reg);
    // Connect to device and send byte
    Wire.write(data); // low byte
    Wire.endTransmission();
}



void i2c_read(int address, byte reg, int count, byte* data)
{
   int i = 0;

   // Send input register address
   Wire.beginTransmission(address);
   Wire.write(reg);
   Wire.endTransmission();
   // Connect to device and request bytes
   int bytesReturned = Wire.requestFrom(address,count);
   if(bytesReturned!=count)
   {
    digitalWrite(13,HIGH);
    Serial.print("E");
    Serial.write(1);
    Serial.println("Bytes returned:");
    Serial.println(bytesReturned);
    return;
   }
   for(i=0;i<count;i++) //Read buffer
   {
     char c = Wire.read(); // receive a byte as character
     data[i] = c;
   }
}

//======================================================
// Initialization functions for MPU9250, HMC5983 and
//  u-blox M8N GPS
//======================================================
void init_MPU9250()
{
  i2c_write(MPU9250_I2C_ADDR, MPUREG_PWR_MGMT_1, BIT_H_RESET); //Reset board
  delay(100);
    
  init_acc();
  init_gyr();
  
}

void init_acc()
{
  i2c_write(MPU9250_I2C_ADDR, MPUREG_ACCEL_CONFIG, BITS_FS_8G); // Set +/- 8G as fullscale
  i2c_write(MPU9250_I2C_ADDR, MPUREG_ACCEL_CONFIG_2, BITS_DLPF_ACC_92HZ); //Set low pass to 92 Hz
  
}


void init_gyr()
{
  i2c_write(MPU9250_I2C_ADDR, MPUREG_GYRO_CONFIG, BITS_FS_2000DPS); //Set 2000 deg./sec as fullscale
  i2c_write(MPU9250_I2C_ADDR, MPUREG_CONFIG, BITS_DLPF_GYR_92HZ); //Set low pass to 92 Hz
}


void init_mag()
{
  i2c_write(HMC5983_I2C_ADDR, HMC5983REG_CONF_A, BITS_CONF_A); //Set 220 Hz and temp. sensor enabled
  i2c_write(HMC5983_I2C_ADDR, HMC5983REG_CONF_B, BITS_CONF_B); //Set +/- 1.3 Ga sensitivity
  i2c_write(HMC5983_I2C_ADDR, HMC5983REG_MODE, BITS_CONT_MODE); //Set continuous mode  
}

//======================================================
// Initialization function for u-blox M8N GPS
//======================================================

void init_gps()
{
  Serial1.begin(19200);  
}

//======================================================
// Read functions for Accelerometer, Gyroscope and Magnetometer
//======================================================

void readAccelerometer(short int* accData)
{
  byte temp[6];
  //Clear 6 spaces
  memset(accData,0,6);
  i2c_read(MPU9250_I2C_ADDR, MPUREG_ACCEL_XOUT_H_R, 6, temp); 
  accData[0]=(short int)((temp[0]<<8)+temp[1]);
  accData[1]=(short int)((temp[2]<<8)+temp[3]);
  accData[2]=(short int)((temp[4]<<8)+temp[5]);
}

void readGyroscope(short int* gyrData)
{
  byte temp[6];
  //Clear 6 spaces
  memset(gyrData,0,6);
  i2c_read(MPU9250_I2C_ADDR, MPUREG_GYRO_XOUT_H_R, 6, temp); 
  gyrData[0]=(short int)((temp[0]<<8)+temp[1]);
  gyrData[1]=(short int)((temp[2]<<8)+temp[3]);
  gyrData[2]=(short int)((temp[4]<<8)+temp[5]);
}

void readMagnetometer(short int* magData)
{
  byte temp[6];
  //Clear 6 spaces
  memset(magData,0,6);
  i2c_read(HMC5983_I2C_ADDR, HMC5983REG_DATA_OUT_X_H, 6, temp); 
//  magData[0]=(short int)((temp[0]<<8)+temp[1]); //Comes in XZY order
//  magData[2]=(short int)((temp[2]<<8)+temp[3]);
//  magData[1]=(short int)((temp[4]<<8)+temp[5]);


  magData[0]=-(short int)((temp[4]<<8)+temp[5]); //Reordered to fit
  magData[1]=-(short int)((temp[0]<<8)+temp[1]); //our personal
  magData[2]=-(short int)((temp[2]<<8)+temp[3]); //standard frame (XYZ)
}


float calc_lon2m(int32_t lat7Deg){
  float lon2m;
  float latDeg = (lat7Deg)/10000000.0;
  latDeg=latDeg/10+1;
  lon2m = (0.0000310726*pow(latDeg,4)-0.000023546*pow(latDeg,3)-0.0173301*pow(latDeg,2)+0.0350466*latDeg+1.09542);
  return lon2m;
}


//======================================================
// Setup, always runs first.
//======================================================

void setup()
{
    delay(1000);
    Serial.begin(115200); // USB connection initialized at 115200 baud.
    Serial2.begin(19200); // Serial connection to acctuator controller initialized to 19200 baud.
    Wire.begin();
    setupRadio();
    init_MPU9250();

    init_mag();
    init_gps();
    //Initialize PID controllers.
    calcPIDParameters_angle(); 
    calcPIDParameters_Distance();
    calcPIDParameters_Velocity();

    // Pin 13 is set to 1 if an error occurs on the i2c bus.
    pinMode(13,OUTPUT);
    digitalWrite(13,LOW);


    dataToSend = 0;
//    dataToSend += SEND_Q;
//    dataToSend +=SEND_GYR;
//    dataToSend +=SEND_ACC;
//    dataToSend +=SEND_MAG;
//    dataToSend +=SEND_GPS;
    
    Serial.println("Setup done");

    

        
}

//======================================================
// Loop, main loop of the program. Runs after setup().
//======================================================
void loop()
{
    timeMicro = micros(); // Save the time at the start of the loop.
    // Used later to calculate a delay to ensure a stable 200 Hz rate.

    if(gyroscope_divider--==0)
    {
      readGyroscope(gyrData);

      // The gyroscope returns the data as 16 bit signed ints between -32768 and 32767 (decimal)
      // corresponding to +/- 2000 deg/s.
      // The factor gyr2radINT convert these to rad/s. >> 15 scales the numbers down so that 
      // there is no risk of overflow in the following calculations. The scaling is done after
      // the multiplication to retain the maximum precision. -35, -3, +12 were added to remove
      // some of the bias. This should probably be done through variables instead.
      gyr[0]=((gyrData[0]-35)*gyr2radINT)>>15;
      gyr[1]=((gyrData[1]-3)*gyr2radINT)>>15;
      gyr[2]=((gyrData[2]+12)*gyr2radINT)>>15;

      limitP((int64_t*)P,(int64_t*)P0); // This was added to avoid 
      // covariance wind-up.

      // The filter uses the gyroscope measurements as inputs for the prediction step.
      predictionStepAHRSINT32(q, P, gyr, stepSize, Rw);
      // q is normalized to keep it at unit length.
      normalizeQINT32(q);

      // = 0 means that this section of code will run every iteration of the main loop i.e. at 200 Hz
      gyroscope_divider=0;
    }
   
    if(accelerometer_divider--==0)
    {
      readAccelerometer(accData);

      acc[0]=((((int32_t)accData[0])*acc2ms2INTx)-accBias[0])>>15;
      acc[1]=((((int32_t)accData[1])*acc2ms2INTy)-accBias[1])>>15;
      acc[2]=((((int32_t)accData[2])*acc2ms2INTz)-accBias[2])>>15;
      
      updateStepAccAHRSINT32(q, P, acc, Ra, g0);
      // q is normalized to keep it at unit length.
      normalizeQINT32(q);

      // = 0 means that this section of code will run every iteration of the main loop i.e. at 200 Hz
      accelerometer_divider=0;
    }
       
    if(!(settings&MAG_OFF) && magnetometer_divider--==0)
    {
      readMagnetometer(magData);
      
      mag[0]=(((((int32_t)magData[0]-magBias[0])*magScale[0])>>11)*mag2muTINT)>>15;
      mag[1]=(((((int32_t)magData[1]-magBias[1])*magScale[1])>>11)*mag2muTINT)>>15;
      mag[2]=(((((int32_t)magData[2]-magBias[2])*magScale[2])>>11)*mag2muTINT)>>15;

      // The velocity as measured by the GPS was included in an attempt to correct the heading from
      // the magnetic declination.
      updateStepMagAHRSINT32(q, P, mag, Rm, m0,*velN*0.001,*velE*-0.001);
      // q is normalized to keep it at unit length.
      normalizeQINT32(q);

      // This sends the current correction to the radio for use in debugging.
      setMagCorrection(getMagCorrection());

      // = 0 means that this section of code will run every iteration of the main loop i.e. at 200 Hz
      magnetometer_divider=0;
    }

    setQuaternion(q); // Send q to the radio for transmision.
    
    if(gps_divider--==0)
    {
      gps_divider=1; // Runs at 100 Hz
      if(!foundGPSMsg) // While no message start has been found search for the start characters.
      {
        uint32_t serialTimer = micros(); // Save the time to ensure that the search stops after 
        // a maximum of 100 us. 
        while(Serial1.available()>1 && micros()-serialTimer<100)
        {
           // Each NAV-PVT message from the GPS start with a "mu" character (ascii 0xB5) followed by 
           // a "b" (ascii 0x62)
           if(Serial1.read()==0xB5)
           {
            if(Serial1.read()==0x62)
            {
              foundGPSMsg=1; // Start has been found
              currentByteGPSMsg = navpvt_temp; // Reset the pointer for the temp storage.
              GPSReadStartTime=micros(); // Save the time so that code can restart if 
              // the message does not arrive within the expected time.
              
              break; // Stop searching
            }
           }
        }
      }
      else // The start of a message has been found.
      {         
            uint32_t serialTimer = micros(); // Save the time.
            while(Serial1.available() && micros()-serialTimer<100)
            {
              if(micros()-GPSReadStartTime>80000) // Has the message taken too long?
              {
//                Serial.println("GPS message timed out");
                foundGPSMsg=0; // Start searching for the next message.
                break;
              }
              *currentByteGPSMsg = Serial1.read(); //Save the bytes read from the serial buffer
              // to the temp storage.
              currentByteGPSMsg++; // Increment the pointer.
              if(currentByteGPSMsg==endOfGPSMsg) // Are we there yet?
              {
                if(1) // Here there should really be a test to see if the checksum at the end of the
                // message is correct.
                {
                  newGPS=1; // A new complete NAV-PVT message has arrived.
                  lastReceivedGPS = millis(); // Time of last valid GPS data.

                  // Copy the data from the temp storage to the array used by the code.
                  for(int i=0;i<98;i++)
                  {
                    navpvt[i]=navpvt_temp[i];
                  }
                  setGPSData(*lon,*lat,*velN,-*velE,lastReceivedGPS); // Send the latest data to the radio.
                  if(*flags & 0x01==1)
                  {
                    GPSFixTimer = millis(); // Time of the last GPS data with a fix (the GPS has found enough
                    // satellites to form an accurate position estimate). 
                  }
                }
                foundGPSMsg=0; // Start searching for the next message.
                break;
              }
            }
        
      }
    }
    
    // This section of code was used in the early stages of debugging to control some parts of the program
    // without having to upload new code. 
    if(dataIn_divider--==0)
    {
        while(Serial.available()>1)
        {
           if(Serial.read()==0x63)
           {
            char c = Serial.read();
//            if(Serial.read()==0x6D)
            if(c==0x6D)
            {
              uint32_t serialTimerConf = micros();
              while(Serial.available()<4)
              {
                if(micros()-serialTimerConf>1000)
                {
                  break;           
                }
              }
              dataToSend=Serial.read()<<24;
              dataToSend+=Serial.read()<<16;
              dataToSend+=Serial.read()<<8;
              dataToSend+=Serial.read();
            }
            else if(c==0x73)
            {
              uint32_t serialTimerConf = micros();
              while(Serial.available()<4)
              {
                if(micros()-serialTimerConf>1000)
                {
                  break;           
                }
              }
              settings=Serial.read()<<24;
              settings+=Serial.read()<<16;
              settings+=Serial.read()<<8;
              settings+=Serial.read();
              if(settings&RESET_Q)
              {
                settings -= RESET_Q;
                resetq((int32_t*)q,(int32_t*)q0);
              }
              if(settings&RESET_P)
              {
                resetP((int64_t*)P,(int64_t*)P0);
                settings -= RESET_P;
              }
            }
           }
        }
      dataIn_divider=9; // 20 Hz
    }

    
#ifndef debug  // The debug command can be defined in the EKF code. This makes it print the result
// from almost every calculation in the EKF (in readable text). Since it does this on the same
// serial bus as the regular data to MATLAB this part of the code is deactivated. Otherwise the debug
// information would be harder to read.

    if(dataOut_divider--==0)
    {
      dataOut_divider=1; // 100 Hz
      
      if(dataToSend&SEND_Q) // The quaternion is sent to MATLAB as a "q" followed by 16 bytes (four 32 bit ints)
      {
        Serial.write('q');
        Serial.write((byte*)q,16);
      }
      if(dataToSend&SEND_GYR)
      {
//         writeGyrMessage();
         Serial.write(gyrMessage,7); // "g"...
      }
      if(dataToSend&SEND_ACC)
      {
//         writeAccMessage();
         Serial.write(accMessage,7); // "a"...
      }
      if(dataToSend&SEND_MAG)
      {
//         writeMagMessage();
         Serial.write(magMessage,7); // "m"...
      }
      if(dataToSend&SEND_GPS)
      {
        
        if(newGPS)
        {
          Serial.print("G");
          Serial.write((byte*)lat,4);
          Serial.write((byte*)lon,4);
          Serial.write(*flags&0x01);
          Serial.write((byte*)hAcc,4);
        }      
      }
      if(dataToSend&SEND_DELAY)
      {
        Serial.print("D");
        Serial.write((byte*)&Delay,2); // The amount of time left at each 200 Hz loop.
      }
      if(dataToSend&SEND_P)
      {
        Serial.print("P");
        Serial.write((byte*)P,128); // The covariance matrix P
      }
      if(dataToSend&SEND_SETTINGS)
      {
        Serial.print("S");
        Serial.write((byte*)&settings,4);
      }

      
    }

    receiveRadioMessages();  // Check the  radio for new messages.
 
    if(control_divider--==0)
    {
      control_divider=9; // 20 Hz
      if(getNewPIDD())
      {
        //Previously AV
        setPIDF_Distance(getPIDD());
      }

      if(getNewPIDV())
      {
        //Previously T
        setPIDF_Velocity(getPIDV());
      }
      
      if(getNewPIDA())
      {
        setPIDF_angle(getPIDA());
      }
      if(getMode()==0)
      {
        sendMode(0); // This writes mode = 0 to the actuator controller.
        setControlData(getMode(),getRudder(),getThrottle()); // Sets the control related parameters in the radio so that
        // the data can be requested by the lead boat. In mode 0 the only important one is the mode.
      }
      else if(getMode()==1 && 1000>(millis()-GPSFixTimer)) // getMode() will return 0 if no target data from the lead boat
      // has been received for some time. The autonomous controller is only used if the mode is 1, there is valid position
      // data from the lead boat and the RescueRunner has GPS fix.
      {

        sendMode(1); // Write mode = 1 to the actuator controller
        posRR[0] = *lat;
        posRR[1] = *lon;
        velRR[0] = ((float)*velN)/1000;
        velRR[1] = ((float)*velE)/-1000;

        int32_t qtemp[4];
        generateq1(qtemp,q); // This creates a quaternion containing only the yaw rotation.
          
        headingQuaternionRR[0] = ((float)qtemp[0])/32768.0;
        headingQuaternionRR[1] = ((float)qtemp[3])/32768.0;
        
        int32_t Qq[3][3];
        QqINT32(Qq,q);

        
        yawRateRR = ((float)(Qq[2][0]*gyr[0] + Qq[2][1]*gyr[1] + Qq[2][2]*gyr[2]))/8388608.0;

        
//        positionVec[6] = ((float)(Qq[2][0]*gyr[0] + Qq[2][1]*gyr[1] + Qq[2][2]*gyr[2]))/1073741824.0;
        updateControl(getPosition(), getVelocity(), getQuaternion(), getYawrate(), posRR,velRR,headingQuaternionRR,yawRateRR,1.110000, calc_lon2m(*lat));
        setControlData(getMode(),-getRudder(),getThrottle());
//        setControlData(14,getRudder(),getThrottle());
        applyControl();
//        sendThrottleRef(getThrottleRef());
//        Serial1.print("MS");
//        Serial1.write((int8_t)1);
//        Serial1.write((int8_t)0);
      }
      else if(getMode()==2)
      {
        Serial.println("mode2");
        sendMode(2);
        sendSteeringRef(getSteeringRef());
        sendThrottleRef(getThrottleRef());
        
        setControlData(getMode(),getRudder(),getThrottle());
//        Serial1.print("MS");
//        Serial1.write((int8_t)1);
//        Serial1.write((int8_t)2);

//        Serial1.print("MS");
//        Serial1.write((int8_t)2);
//        Serial1.write(getSteeringRef());
//
//        Serial1.print("MS");
//        Serial1.write((int8_t)3);
//        Serial1.write(getThrottleRef());
      }

      else if(getMode()==3 && 1000>(millis()-GPSFixTimer))
      {
          //test GPS
        
//        Serial.println("mode1");
        sendMode(1);
//        positionVec[0] = ((float)(*lat))/10000000;
//        positionVec[0] = *lat;
////        positionVec[1] = ((float)(*lon))/10000000;
//        positionVec[1] = *lon;
//        positionVec[2] = ((float)*velN)/1000;
//        positionVec[3] = ((float)*velE)/-1000;
//        positionVec[4] = ((float)q[0])/32768.0;
//        positionVec[5] = ((float)q[3])/32768.0;
        posRR[0] = *lat;
        posRR[1] = *lon;
        velRR[0] = ((float)*velN)/1000;
        velRR[1] = ((float)*velE)/-1000;
        headingQuaternionRR[0] = ((float)q[0])/32768.0;
        headingQuaternionRR[1] = ((float)q[3])/32768.0;
        int32_t Qq[3][3];
        QqINT32(Qq,q);

        
        yawRateRR = ((float)(Qq[2][0]*gyr[0] + Qq[2][1]*gyr[1] + Qq[2][2]*gyr[2]))/8388608.0;

        
        // Calculate new control outputs.
        updateControl(getPosition(), getVelocity(), getQuaternion(), getYawrate(), posRR,velRR,headingQuaternionRR,yawRateRR,1.110000, calc_lon2m(*lat));
        // Update the copies in the radio code so that the lead boat can request them.
        setControlData(getMode(),-getRudder(),getThrottle());
        // Write the steering angle and throttle reference to the actuator controller. This is done as a separate function so it
        // can be commented out.
        applyControl();

      }
    }
  #endif  
    Delay = 5000-(unsigned int)(micros()-timeMicro); // Calculate the delay needed to ensure a 200 Hz rate.
    if(Delay<5000)
    {
      delayMicroseconds(Delay);
    }
    
    
}
