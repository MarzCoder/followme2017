#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include <arduino.h>

void vecPlusMatxVec(int32_t result[4], int32_t x[4], int32_t A[4][4]);

void X_plus_AxX_INT32(int32_t result[4], int32_t x[4], int32_t A[4][4]);

void calc_P_plus_SP_INT32(int64_t result[4][4], int32_t A[4][4], int64_t B[4][4]);

void calc_temp_P_INT32(int64_t result[4][4], int64_t A[4][4], int32_t B[4][4]);

void matMult3x3(int32_t result[3][3], int32_t A[3][3], int32_t B[3][3]);

void matMult3x3Transpose(int32_t result[3][3], int32_t A[3][3], int32_t B[3][3]);

void X_plus_AxX_INT64(int64_t result[4], int64_t x[4], int64_t A[4][4]);

void calc_SRINT32(int64_t result[4][3], int32_t A[4][3], int64_t B[3][3]);

void calc_SRS_INT32(int64_t result[4][4], int64_t A[4][3], int32_t B[4][3]);

void calc_P_plus_SRS_INT32(int64_t result[4][4], int64_t A[4][3], int32_t B[4][3]);

void calc_hdINT32(int32_t result[3][4], int32_t A[12][3], int32_t x[3]);

void calc_hINT32(int32_t result[3], int32_t A[12][3], int32_t x[3]);

void calc_h_magINT32(int32_t result[3], int32_t A[12][3], int32_t x[3]);

void calc_Phd_transposeINT32(int64_t result[4][3], int64_t A[4][4], int32_t B[3][4]);

void calc_SINT32(int64_t result[3][3], int32_t A[3][4], int64_t B[4][3], int64_t C[3][3]);

void calc_S_magINT32(int64_t result[3][3], int32_t A[3][4], int64_t B[4][3], int64_t C[3][3]);

void calc_KINT32(int32_t result[4][3], int64_t A[4][3], int64_t B[3][3]);

void calc_K_magINT32(int32_t result[4][3], int64_t A[4][3], int64_t B[3][3]);

void calc_diffAcchINT32(int32_t* diff,int32_t* x, int32_t* y);

void update_qINT32(int32_t result[4],int32_t A[4][3], int32_t x[3]);

void update_q_magINT32(int32_t result[4],int32_t A[4][3], int32_t x[3]);

void calc_KSK_INT32(int32_t result[4][4], int32_t A[4][3], int32_t B[4][3]);

void update_PINT32(int64_t result[4][4], int64_t A[4][3], int32_t B[4][3]);

void update_P_magINT32(int64_t result[4][4], int64_t A[4][3], int32_t B[4][3]);


//void B_plus_AxB_INT32(int32_t result[4][4], int32_t A[4][4], int32_t B[4][4]);

//void A_plus_AxB_transposeINT32(int32_t result[4][4], int32_t A[4][4], int32_t B[4][4]);




