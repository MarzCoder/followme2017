// globals.h

#ifndef _GLOBALS_h
#define _GLOBALS_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#define HMIserial Serial //The serial port used to send data to Human Machine Interface, i.e. matlab

/*=================================================================
TELL THE COMPILER WHICH VEHICLE IT IS*/

//#define LEADER_BOAT
//#undef LEADER_BOAT
#define RESCUE_RUNNER
//#undef RESCUE_RUNNER

/*=================================================================*/















#ifdef LEADER_BOAT
const char vehicleID[] = { "Leader boat" }; 
#else 
const char vehicleID[] = { "Rescue Runner" }; 
#endif

///THIS  SHIT SHOULDN'T BE GLOBAL, PUT HERE IN A QUICK CLEANUP


extern int32_t q[4];
extern int32_t q0[4];// = { 32768, 0, 0, 0 }; // Initial orientation guess.
// Covariance matrix belonging to the quaternion.
extern int64_t P[4][4];// = { { 70368744, 0, 0, 0 }, { 0, 70368744, 0, 0 }, { 0, 0, 70368744, 0 }, { 0, 0, 0, 70368744 } };
//Initial values.
extern int64_t P0[4][4];// = { { 70368744, 0, 0, 0 }, { 0, 70368744, 0, 0 }, { 0, 0, 70368744, 0 }, { 0, 0, 0, 70368744 } };


extern int32_t gyr[3];// = { 0, 0, 0 }; // Array that holds the latest gyroscope measurements (xyz).
// Covariance matrix for the gyroscope measurements
extern int64_t Rw[3][3];// = { { 175644275, -8996453, 5426 }, { -8996453, 137892623, -1445288 }, { 5426, -1445288, 108339063 } };

extern int32_t acc[3]; // Array that holds the latest accelerometer measurements (xyz).
// Covariance matrix for the accelerometer measurements
extern int64_t Ra[3][3];// = { { 21805796051, 494807613, 431278077 }, { 494807613, 19206138668, 2577081565 }, { 431278077, 2577081565, 57967695683 } };
extern int32_t g0[3];// = { 0, 0, 10056 }; // The nominal gravity vector
// Accelerometer bias
extern int32_t accBias[3];// = { 115343, 188744, -193987 };

extern int32_t mag[3]; // Array that holds the latest magnetometer measurements (xyz).
// Covariance matrix for the magnetometer measurements
extern int64_t Rm[3][3];// = { { 6871948, 0, 0 }, { 0, 6871948, 0 }, { 0, 0, 6871948 } };
extern int32_t m0[3];// = { 256, 0, 0 }; // The nominal horizontal component of the Earth's magnetic field.
// Bias and scale error of the magnetometer
extern int32_t magBias[3];// = { 158, -18, 72 }; //scale 2^0
extern int32_t magScale[3];// = { 32072, 31105, 35431 }; //scale 2^15

// The step size of the filter 10486 corresponds to 0.0050001 seconds
extern int32_t stepSize;// = 10486;


//These variables are used to temporarily store the position and orientation data before its sent to the RR.
//They should probably not be global.
extern int32_t posLB[2];
extern float	velLB[2];
extern float	headingQuaternionLB[2];
extern float	yawRateLB;



#endif

