

//Libraries

#include <printf.h>
#include <Wire.h>
#include <SPI.h>
#include <stdio.h>
#include <stdint.h>
#include <Arduino.h>

//User defined 
#include "settings.h"
#include "MPU9250.h"
#include "I2C.h"
#include "globals.h"
#include "MPU9250_definitions.h"
#include "HMC5983_definitions.h"
#include "QuaternionFunctionsInt.h"
#include "MatrixOperations.h"

#include "protoctol.h"
#include "GPS.h"
#include "userInterface.h"			
#include "settings.h"
#include "mainMode.h"
#include "radio.h"
#include "trackingSystem.h"


/*==================================================
Static objects */

radio			wireLessRadio(pin::radio::CE, pin::radio::CSN);
UI				matlabInterface(&HMIserial);
uBloxGPS		GPS(&GPSserialPort, baud::GPS);
mainMode		vehicleMainMode;
trackingSystem	tracking(wayPointParams::distanceFilter); 


//TODO: There will be loads of flags that we want to indicate later probably
enum LBmainFlags
{
	COMMUNICATION_ERROR_WITH_RR = (1<<0),
	SOMEOVERFLOW				= (1 << 1),
	MAGNETOMETER_ON				= (1<<2),

};

uint16_t mainFlags = 0; 

/*============================================================
 * This is the code that runs on the lead boat arduino due.
 * It performs multiple tasks:
 * 
 * It reads commands sent over USB from MATLAB. These determine the mode
 * of the system. It also sends data back to MATLAB. What is sent is
 * determined by the "dataToSend" variable configured in setup.
 * 
 * It samples the gyroscope, accelerometer and magnetometer and fuses
 * the measurements with an EKF to form an orientation estimate. 
 * 
 * It reads the ublox binary measages from the GPS receiver from the serial
 * buffer. To avoid having to wait for the whole message to arrive it reads
 * it in small pieces at a time.
 * 
 * It sends messages to the RR (RescueRunner) arduino due through the nRF24L01+ radio transceiver.
 * These contain the lead boat's position, velocity and orientation as well as the
 * current mode. It can also request data from the RR. This is then sent on to MATLAB
 * to be used for debugging and visualization.  
 * 
 * 
 * The main loop of the code runs at a rate of 200 Hz. This is because of the  
 * orientation filter needing to update often to minimize linearization errors.
 * This means that each loop iteration cannot take more than 0.005 seconds.
 * The rate of the other tasks are decided by dividers which takes the 200 Hz
 * rate and divides it by an integer number.
 *  
 * ===========================================================
 */



// This object inteprets the serial messages from MATLAB. The function
// "checkMessages()" is run regularly to ensure that the serial buffer
// does not overflow.


//======================================================
// Rate control variables initialization
//======================================================

short int gyroscope_divider			= 0;
short int accelerometer_divider		= 0;
short int magnetometer_divider		= 0;
short int gps_divider				= 0;
short int dataOut_divider			= 0;
short int dataOut20Hz_divider		= 0;
short int radio_divider				= 0;
short int radio_slot1_divider		= 0;
short int mh_divider				= 0;
short int position_divider			= 0;

unsigned short int Delay;
unsigned long timeMicro;

int32_t settings = 0; //Settings for reading the magnetometer or not etc etc

//======================================================
// Setup, always runs first.
//======================================================

void setup()
{
    delay(1000);
    HMIserial.begin(baud::UI);  // USB connection initialized at 115200 baud.
	GPSserialPort.begin(baud::GPS); 
    Wire.begin();
	wireLessRadio.setup(); 
    init_MPU9250();
	GPS.init();
    
   // dataToSend = 0;
//    dataToSend += SEND_Q;
//    dataToSend +=SEND_GYR;
//    dataToSend +=SEND_ACC;
//    dataToSend +=SEND_MAG;
    //dataToSend +=SEND_GPS;
//    dataToSend +=SEND_P;
//    dataToSend +=SEND_SETTINGS;

   // dataToSend +=SEND_RR_CONTROL_OUT;
    //dataToSend +=SEND_POS;
    //dataToSend +=SEND_HEADING;
   // dataToSend +=SEND_RADIO_STATUS;
   // dataToSend +=SEND_MAGCORRECTION;

    settings = 0;
//    settings +=MAG_OFF;

    // Pin 13 is set to 1 if an error occurs on the i2c bus.
    pinMode(13,OUTPUT);
    digitalWrite(13,LOW);
    
	const char stars[] = { "********************************************\n" };

	HMIserial.print(stars);
    HMIserial.print("Setup done for ");
	HMIserial.println(vehicleID);
	HMIserial.print(stars);
	
}

//======================================================
// Loop, main loop of the program. Runs after setup().
//======================================================



void loop()
{
	
	//wireLessRadio.loopBackTest(); 

    timeMicro = micros(); // Save the time at the start of the loop.
    // Used later to calculate a delay to ensure a stable 200 Hz rate.

	/*==========================================================
	//ACCELEROMETER
	===========================================================*/
    if(gyroscope_divider--==0)
    {
      //readGyroscope(gyrData);

      // The gyroscope returns the data as 16 bit signed ints between -32768 and 32767 (decimal)
      // corresponding to +/- 2000 deg/s.
      // The factor gyr2radINT convert these to rad/s. >> 15 scales the numbers down so that 
      // there is no risk of overflow in the following calculations. The scaling is done after the
      // to retain the maximum precision. -35, -3, +12 were added to remove some of the bias.
      // This should probably be done through variables instead.
      
      gyr[0]=((gyrData[0])*gyr2radINT)>>15;
      gyr[1]=((gyrData[1])*gyr2radINT)>>15;
      gyr[2]=((gyrData[2])*gyr2radINT)>>15;

      limitP((int64_t*)P,(int64_t*)P0); // This was added to avoid 
      // covariance wind-up.
      
      // The filter uses the gyroscope measurements as inputs for the prediction step.
      predictionStepAHRSINT32(q, P, gyr, stepSize, Rw);
      // q is normalized to keep it at unit length.
      normalizeQINT32(q);

      // = 0 means that this section of code will run every iteration of the main loop i.e. at 200 Hz
      gyroscope_divider=0;
    }
   

	/*==========================================================
	//GYRO
	*/
    if(accelerometer_divider==0)
    {
      //readAccelerometer(accData);

      acc[0]=((((int32_t)accData[0])*acc2ms2INTx)-accBias[0])>>15;
      acc[1]=((((int32_t)accData[1])*acc2ms2INTy)-accBias[1])>>15;
      acc[2]=((((int32_t)accData[2])*acc2ms2INTz)-accBias[2])>>15;
      
      updateStepAccAHRSINT32(q, P, acc, Ra, g0);
      // q is normalized to keep it at unit length.
      normalizeQINT32(q);
      
      // = 0 means that this section of code will run every iteration of the main loop i.e. at 200 Hz
      accelerometer_divider=0;
    }
       
	/*==========================================================
	//MAGNETOMETER
	*/
    if( (mainFlags & MAGNETOMETER_ON) && magnetometer_divider--==0)
    {
      //GPS.readMagnetometer(magData);
      
      mag[0]=(((((int32_t)magData[0]-magBias[0])*magScale[0])>>11)*mag2muTINT)>>15;
      mag[1]=(((((int32_t)magData[1]-magBias[1])*magScale[1])>>11)*mag2muTINT)>>15;
      mag[2]=(((((int32_t)magData[2]-magBias[2])*magScale[2])>>11)*mag2muTINT)>>15;
      updateStepMagAHRSINT32(q, P, mag, Rm, m0);
      // q is normalized to keep it at unit length.
      normalizeQINT32(q); 

      // = 0 means that this section of code will run every iteration of the main loop i.e. at 200 Hz
      magnetometer_divider=0;
    }

	/*==========================================================
	 MAIN MODE update - crucial and safety critical*/

	/*
	vehicleMainMode.update(	matlabInterface.getUserRequestedMode(),
							vehicleRadio.getConnectedVehiclesMode(),
							GPS.getTimeOfLastData(),
							matlabInterface.timeOfLastUImessage() );*/

	
	/*==========================================================
	//GPS
	*/
    if(gps_divider--==0)
    {
		static float x, y;

		if (GPS.poll())
		{
			/*
			HMIserial.print("New GPS: \t X: ");
			HMIserial.print(GPS.getX());
			HMIserial.print("New GPS: \t Y: ");
			HMIserial.println(GPS.getY());
			 */

			
			#ifdef LEADER_BOAT
				//tracking.generateNextWayPoint(GPS.getX(), GPS.getY());

			#elif defined RESCUE_RUNNER
			//SEE IF WE SHOULD UPDATE THE WAYPOINT
			#endif
		}

#ifdef LEADER_BOAT
		tracking.generateNextWayPoint(x += 5, y += 5); //DEBUG: SImulate that were moving
#endif

		//gps_divider=1; // Runs at 100 Hz
		gps_divider = 100; 
		//GPS.readMagnetometer()
		//GPS.poll(); 
    
    }

	/*========================================================================
		Sent/read matlab communication 
	*/
    // This reads the serial buffer containing messages from MATLAB.
    if(mh_divider--==0)
    {
		/*
		HMIserial.print("X:"); 
		HMIserial.print(GPS.getX()); 
		HMIserial.print("\t\tY:");
		HMIserial.print(GPS.getY()); 
		HMIserial.print("\t\tAccuracy [m]: ");
		HMIserial.print(GPS.getHorisontalAccuracy());
		HMIserial.print("\t\tLong: ");
		HMIserial.print(GPS.getLongitude());
		HMIserial.print("\t\tLat: ");
		HMIserial.println(GPS.getLatitude());*/


		/*
		HMIserial.print(radioToRescueRunner.formatLastPacketAsASCII()); //Send a mirror of the last packet we received
		HMIserial.print(",");
		HMIserial.print(mainFlags); 
		HMIserial.print(", \tLat: ");
		HMIserial.print(GPS.getLatitude()); 
		HMIserial.print(",\t Long: ");
		HMIserial.print(GPS.getLongitude());
		HMIserial.println(); */
	
		//Position, Orientation, etc
		//mh_divider = 9; // 20 Hz
		mh_divider = 99; 
    }


	/*===================================================
	Communicate with rescue runner. */

	if (radio_divider-- == 0)
	{
		wireLessRadio.poll(); 
		radio_divider = 99; 
	}

	/*===============================================
	 Handle the current Main mode*/

	//TODO: If any of the two vehicles is in manual mode, both should be set in 


#ifdef debug  
	// The debug command can be defined in the EKF code. This makes it print the result
// from almost every calculation in the EKF (in readable text). Since it does this on the same
// serial bus as the regular data to MATLAB this part of the code is deactivated. Otherwise the debug
// information would be harder to read.

    if(dataOut_divider--==0)
    {
      dataOut_divider=1; // 100 Hz

      if(dataToSend&SEND_Q) // The quaternion is sent to MATLAB as a "q" followed by 16 bytes (four 32 bit ints)
      {
        Serial.write('q');
        Serial.write((byte*)q,16);
      }
      if(dataToSend&SEND_GYR)
      {
         Serial.write(gyrMessage,7); // "g"...
      }
      if(dataToSend&SEND_ACC)
      {
         Serial.write(accMessage,7); // "a"...
      }
      if(dataToSend&SEND_MAG)
      {
         Serial.write(magMessage,7); // "m"..
      }
      if(dataToSend&SEND_GPS)
      {
        
        if(newGPS)
        {
          Serial.print("G");
          Serial.write((byte*)lat,4);
          Serial.write((byte*)lon,4);
          Serial.write(*flags&0x01);
          Serial.write((byte*)hAcc,4);
        }      
      }
      if(dataToSend&SEND_DELAY)
      {
        Serial.print("D");
        Serial.write((byte*)&Delay,2); // The amount of time left at each 200 Hz loop.
      }
      if(dataToSend&SEND_P)
      {
        Serial.print("P");
        Serial.write((byte*)P,128); // The covariance matrix P
      }

      if(dataToSend&SEND_SETTINGS)
      {
        Serial.print("S");
        Serial.write((byte*)&settings,4);
      }

      if(dataOut20Hz_divider--==0)
      {
        dataOut20Hz_divider=4; //100 Hz/5 = 20 Hz
        
        if(dataToSend&SEND_RR_CONTROL_OUT)
        {
          Serial.print("c");
          Serial.write(getControlOut(),3); // The control outputs from the autonomous controller
          // are requested over the radio link in 20 Hz intervals in the code above. This reads
          // the latest data and writes it to the serial bus (USB) to MATLAB.
          
        }
        if(dataToSend&SEND_POS)
        {
          Serial.print("p");
          Serial.write((byte*)lon,4);
          Serial.write((byte*)lat,4);
          Serial.write((byte*)velN,4);
          Serial.write((byte*)velE,4);
          Serial.write((byte*)lastReceivedGPS,4);
          Serial.write(getGPSData(),20);
        }
        if(dataToSend&SEND_HEADING)
        {
          Serial.print("h");
          Serial.write((byte*)q,4);
          Serial.write((byte*)(q+1),4);
          Serial.write((byte*)(q+2),4);
          Serial.write((byte*)(q+3),4);
          Serial.write(getHeading(),16);
        }
        if(dataToSend&SEND_RADIO_STATUS)
        {
          Serial.print("r");
          Serial.write(getRadioStatus());
        }
        if(dataToSend&SEND_MAGCORRECTION)
        {
          Serial.print("M");
          Serial.write((byte*)getMagCorrection(),4);
        }
      }
            
      
      
    }
 //REMOVE? 
#endif 
    
	/*===============================================================================
	Loop delay*/

    Delay = 5000-(unsigned int)(micros()-timeMicro); // Calculate the delay needed to ensure a 200 Hz rate.
//    Serial.println(Delay);
    if(Delay<5000)
    {
      delayMicroseconds(Delay);
    }
    
}

