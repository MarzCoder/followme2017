// mainMode.h

#ifndef _MAINMODE_h
#define _MAINMODE_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif


/*=========================================================
Handles the main mode of the vehicle, if it should be autonomous,
by hand controller etc. 
==========================================================*/
class mainMode
{
public:
	/*The most important mode that determines what the system does*/
	enum mode
	{
		MAINMODE_Manual,			/*Manually by the human driver*/
		MAINMODE_Following,			/*The rescue runner follows the leaderboat*/
		MAINMODE_GPStrajectory,		/*The rescue runner follows a GPS trajectory*/
		MAINMODE_Handcontroller,	/*An external handcontroller is connected to a computer that is connected to this*/
		MAINMODE_Unknown,			/*Used when the connected vehicles modes is unknown */
	};

	
	mode update(mode userRequest,				/*The user sends a mode request via the UI, i.e. matlab */
				mode theOtherVehiclesMode,		/*If the other vehicle set its mode to manual, we will do so too*/
				uint32_t timeOfLastGPS,			/*GPS time out is critical, sets to manual usually*/
				uint32_t lastUserrequest);		/*The UI should send a req. continually. If didn't get any, set to manual*/

	/*If the GPS hasn't received any new valid value within this time, we change mode to manually*/

	struct timeOuts
	{
		static constexpr uint32_t GPS = 5000;
		static constexpr uint32_t UI= 2000;

	};
	

private:
	mode currentMainMode; 
	bool GPShasTimedOut= true;
};


#endif

