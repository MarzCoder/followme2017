// UI.h

#ifndef _HMI_h
#define _HMI_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include "mainMode.h"

/*All Human Machine interaction eg. with matlab etc is handled here*/
class UI
{

public:
	//Constructor
	UI();        //Uses Serial by default

	UI(HardwareSerial* hwSerial);   //Uses the referenced serial interface

	int       checkMessages(uint16_t msgTimeOut); //Reads the serial buffer and processes the messages. This

	mainMode::mode getUserRequestedMode(); 

	uint32_t timeOfLastUImessage(); 

	enum messageType
	{		
			MESSAGETYPE_NoMsg,
			MESSAGETYPE_Mode,
			MESSAGETYPE_Steering,
			MESSAGETYPE_Throttle,
			MESSAGETYPE_PID_D_update,
			MESSAGETYPE_PID_A_update,
			MESSAGETYPE_PID_V_update
	};

	//function need to be called at regular intervalls to keep
	//the control signals up to date. The argument msgTimeOut
	//specifies the time the function is allowed to run for.

	/*Sends values to the UI (presum. MATLAB interface) using CSV*/
	//void	  send(float & value); 


	uint8_t   getMode();                            //Returns the mode.
	int8_t    getSteeringAngleRef();                //Returns the steering angle reference.
	uint8_t   getThrottleRef();                     //Returns the throttle reference.
	void      sendSteeringAngle(int8_t angle);      //Sends the current steering angle.
	uint8_t   getNewPIDD();
	uint8_t   getNewPIDA();
	uint8_t   getNewPIDV();
	byte*     getPIDD();
	byte*     getPIDA();
	byte*     getPIDV();

private:
	uint8_t   mode;                   //Holds the current mode (0 by default).
	int8_t    steeringAngleRef;       //Holds the current steering angle reference (0 by default).
	uint8_t   throttleRef;            //Holds the current throttle reference (0 by default).   
	int       messagePosition;        //Used to keep track of where in the message we are.
	uint8_t   messageID;              //Holds the ID of the latest message.
	uint8_t   lastByte;               //Last byte read from the serial buffer.
	byte      PID_parametersD[16];
	byte      PID_parametersA[16];
	byte      PID_parametersV[16];
	uint8_t   newPIDD;
	uint8_t   newPIDA;
	uint8_t   newPIDV;
	//    uint8_t   temp[4];                //Not used
	//    uint8_t*  tempPointer;            //Not used

	uint32_t  timeOfLastModeMessage;  //Holds the time of the last mode message. If the mode has not been updated
	//for a while the mode is changed back to 0.
	HardwareSerial* _HardSerial;      //Reference to the serial interface to use (Serial2 by default).
	//19200 baud is used.

	// Two 32 bit variables used to control what data is sent to MATLAB and
	// other settings (currently only magnetometer update on/off).
	int32_t dataToSend = 0;
	

};



extern uint8_t accMessage[];

extern uint8_t gyrMessage[]; 

extern uint8_t magMessage[];


#endif

