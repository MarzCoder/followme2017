// trackingSystem.h

#ifndef _WAYPOINTS_h
#define _WAYPOINTS_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include "globals.h"
#include "settings.h"
#include "QueueArray.h"

static const uint8_t sizeOfWaypointBuffer = 100; //Number of waypoints to keep in buffer

class trackingSystem
{
public: 
	trackingSystem(float _minDist) : minDist(_minDist) {};

	

	float distanceToLast(float x, float y);

	//The information a waypoint contains
	struct waypoint
	{
		uint8_t ID; //Is added by sender when transmitting a waypoint
		float X, Y;
		uint32_t timeStamp;
		uint32_t number; //The number in succession a waypoint has
	};

	void printWayPoint(waypoint T); 

#ifdef LEADER_BOAT
	void generateNextWayPoint(float x, float y);
	QueueArray<waypoint, 100> wayPointsToSend;
#elif defined RESCUE_RUNNER
	waypoint getNextWaypoint(const float& x, const float& y, float clearance, float distToLeaderboat );
	QueueArray<waypoint, 100> wayPointsToClear;
#endif
	

private:
	/*The minimum distance waypoints can be from one another*/
	const float minDist; 

#ifdef LEADER_BOAT
	waypoint lastWaypoint; 
#elif defined RESCUE_RUNNER
	waypoint lastWaypoint;
#endif


};






#endif

