// 
// 
// 

#include "GPS.h"
#include "HMC5983_definitions.h"
#include "I2C.h"
#include "globals.h"

#define degToRad(x) (float(x)* float(0.0174533))


/*@param __GPSserial - The serial port used to interface with the GPS
  @param - the baud of this serial port. Make sure its the same on the GPS!
  @param _lat0 - the nominal latitude where the GPS is to operate, used to do conversion to polar coordinates.
				Defaults to the latitude of Gothenburg. */
uBloxGPS::uBloxGPS(HardwareSerial* __GPSserial, uint32_t __baud, float _lat0):
GPSserial(__GPSserial), 
lat0(_lat0),
baud(__baud)
{

}

void uBloxGPS::init()
{
	setAllResultsNAN(); 
	GPSserial->begin(baud);
	initMagnetometer();
}

float uBloxGPS::getLongitude(){ return currentResult.lon; }

float uBloxGPS::getLatitude() {	return currentResult.lat;}

float uBloxGPS::getHorisontalAccuracy(){ return float(*hAcc)*0.001; }

float uBloxGPS::getX(){ return currentResult.x; }

float uBloxGPS::getY(){ return currentResult.y; }

uint32_t uBloxGPS::getTimeOfLastData(){ return currentResult.timeStamp; }

bool uBloxGPS::poll()
{
	bool gotNewMessage = false; 

	/*============================================================
		Look for the startbytes of a GPS message*/

	if (!foundGPSMsg)
	{
		uint32_t serialTimer = micros(); // Save the time to ensure that the search stops after
		// a maximum of 100 us.  
		while (GPSserial->available() > 1 && micros() - serialTimer<100)
		{
			// Each NAV-PVT message from the GPS start with a "mu" character (ascii 0xB5) followed by 
			// a "b" (ascii 0x62)
			if (GPSserial->read() == 0xB5)
			{
				if (GPSserial->read() == 0x62)
				{
					foundGPSMsg = true; // Start has been found
					messageIndex = 0; 
					GPSReadStartTime = micros(); // Used for a timeout
					//poll();						 //Do a recursive call at once to start decoding the message
					//HMIserial.println("Got GPS message!");

					break; // Stop searching
				}
			}
		}
	}
	else 
	{
	/*======================================================
		A message was found, save it down byte for byte*/

		while (GPSserial->available() > 0)
		{
			int8_t inbyte = GPSserial->read();

			messageBuffer[messageIndex++] = uint8_t(inbyte);  
		
			/*==============================================
				Got complete message*/
			if (messageIndex >= GPSmessageLength)
			{
					newValidGPSdata = false;
					/*=================================
					Do various error checks, starting with checksum, before saving data. Else we keep our old data*/
					//if(calculateCheckSum())
					if (true)
					{	
						if (FLAG_gnssFixOk)
						{ //As of what we understood, this flag is the most imporant
							if (!checkIfThereAreAnyOutliers())
							{  
								convertAndSaveResults(); 
								newValidGPSdata				= true; 
								gotNewMessage				= true; 
							}
							else
							{
								HMIserial.println("<GPS> Rejected outlier!");
							}
						}
						else
						{
							HMIserial.println("<GPS> No fix!");
						}
					}
					else
					{	
						HMIserial.println("<GPS> Checksum error!");
					}
					// Start searching for the next message.
					messageIndex = 0;
					foundGPSMsg = false;
					break; 
			} //END OF CHECK FOR MESSAGE LENGTH
			/*================================================================
			Timeout*/
			else if ((micros() - GPSReadStartTime) > timeoutUs) // Has the message taken too long?
			{
				HMIserial.println("<GPS> Reached timeout");
				foundGPSMsg = false; // Start searching for the next message.
				messageIndex = 0;
				break;
			}
		}//END OF WHILE LOOP
	
	}

	return gotNewMessage; 
}

void uBloxGPS::readMagnetometer(short int* magData)
{
	byte temp[6];
	//Clear 6 spaces
	memset(magData, 0, 6);
	i2c_read(HMC5983_I2C_ADDR, HMC5983REG_DATA_OUT_X_H, 6, temp);
	//  magData[0]=(short int)((temp[0]<<8)+temp[1]); //Comes in XZY order
	//  magData[2]=(short int)((temp[2]<<8)+temp[3]);
	//  magData[1]=(short int)((temp[4]<<8)+temp[5]);


	magData[0] = -(short int)((temp[4] << 8) + temp[5]); //Reordered to fit
	magData[1] = -(short int)((temp[0] << 8) + temp[1]); //our personal
	magData[2] = -(short int)((temp[2] << 8) + temp[3]); //standard frame (XYZ)

}


/*________________________-PRIVATE*/

/*___All fault detection is cleared, and we save a new result*/
bool uBloxGPS::convertAndSaveResults()
{
		currentResult.timeStamp = millis(); 
		currentResult.lat		= float(*lat)*0.0000001;  //Was in 10^-7 degrees
		currentResult.lon		= float(*lon)*0.0000001;  //Was in 10^-7 degrees
		currentResult.velE		= float(*velE)*0.001;	  //Was in mm/s
		currentResult.velN		= float(*velN)*0.001;	  //Was in mm/s
		currentResult.speed		= sqrt(pow(currentResult.velE, 2) + pow(currentResult.velN, 2));  //Gives the vector norm

		//Do conversion to cartesian that will be valid close to gothenburg
		currentResult.x = r*degToRad(currentResult.lon) * cos(degToRad(lat0));
		currentResult.y = r*degToRad(currentResult.lat);

		return true; 
}


void uBloxGPS::setAllResultsNAN()
{
	currentResult.lat	= NAN;
	currentResult.lon	= NAN;
	currentResult.velE	= NAN;
	currentResult.velN	= NAN;
	currentResult.speed = NAN;
	currentResult.x		= NAN;
	currentResult.y		= NAN;
}

//From "UBX checksum" in the Ublox 8 manual 
// Doesnt work yet :(
bool uBloxGPS::calculateCheckSum()
{
	uint8_t CK_A_calc = 0, CK_B_calc = 0; 

	for(uint8_t i = 0; i<GPSmessageLength-LENGTH_CHKSUM; i++)
	{
		CK_A_calc = CK_A_calc + messageBuffer[i];
		CK_B_calc = CK_B_calc + CK_A_calc; 
	}

	if (	(CK_A_calc == get_CK_A(messageBuffer) )
		&&  (CK_B_calc == get_CK_B(messageBuffer) )	)
	{
		return true; 
	}
	else
	{
		return false; 
	}

}

//Returns true if an outlier is detected 
bool uBloxGPS::checkIfThereAreAnyOutliers()
{

	if (*hAcc > outlier::accuracyEstimate || *vAcc > outlier::accuracyEstimate)
	{
		return true; 
	}
	else
	{
		return false; 
	}


	//TODO: Base it on rate of change of raw data instead, and not specific to a location 
	/*
	if (	(currentResult.lat > outlier::lat::max)
		||	(currentResult.lat < outlier::lat::min))
	{
		return true; 
	}
	else if ( (currentResult.lon > outlier::lon::max)
		   || (currentResult.lon < outlier::lon::min))
	{
		return true; 
	}
	else
	{
		return false; 
	}
	*/

}


void uBloxGPS::initMagnetometer()
{
	i2c_write(HMC5983_I2C_ADDR, HMC5983REG_CONF_A, BITS_CONF_A); //Set 220 Hz and temp. sensor enabled
	i2c_write(HMC5983_I2C_ADDR, HMC5983REG_CONF_B, BITS_CONF_B); //Set +/- 1.3 Ga sensitivity
	i2c_write(HMC5983_I2C_ADDR, HMC5983REG_MODE, BITS_CONT_MODE); //Set continuous mode  
}

//float uBloxGPS::degToRad(float degrees)
//{
	//return 

//}



