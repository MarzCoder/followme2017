#include "MessageHandler.h"

MessageHandler::MessageHandler()
{
  mode = 0;
  steeringAngleRef = 0;
  throttleRef = 0;
  messagePosition = 0;
  messageID = 0;
  lastByte = 0;
  PID_parametersD;
  PID_parametersA;
  PID_parametersV;
  newPIDD =0;
  newPIDA=0;
  newPIDV=0;
  timeOfLastModeMessage = 0;
  _HardSerial = &Serial2;
  _HardSerial -> begin(115200);
}

MessageHandler::MessageHandler(HardwareSerial* hwSerial){
  mode = 0;
  steeringAngleRef = 0;
  throttleRef = 0;
  messagePosition = 0;
  messageID = 0;
  lastByte = 0;
  PID_parametersD;
  PID_parametersA;
  PID_parametersV;
  newPIDD =0;
  newPIDA=0;
  newPIDV=0;
  timeOfLastModeMessage = 0;
  _HardSerial = hwSerial;
//  _HardSerial -> begin(19200);
}

/////////////////////////////////////////////
/// Check for a new message bytes for x microseconds
int MessageHandler::checkMessages(uint16_t msgTimeOut)
{  
  uint32_t timer = micros();

  byte lastByte;

  int messagesReceived = 0;

  ///TODO: Change to while timer && available ?? 
  while(micros()-timer<msgTimeOut)
  {
    if(_HardSerial -> available())
    {

      lastByte = _HardSerial -> read();

      switch(messagePosition)
	  {
        case 0:
			  if(lastByte==0x4D)  //0x4D=M
					messagePosition = 1;		//First part of message start detected.
			  break;
        case 1:
			  if(lastByte==0x53)  //0x53=S
			  {
					messagePosition = 2;        //Second part of message start detected.
			  }
			  else
			  {
					Serial.println("<LB> Got invalid message!");
					messagePosition = 0;		//Error, go back
			  }
			  break;
        case 2:
			  messageID			= lastByte;
			  messagePosition	= 3;
			  break;
        default:
			  /*////////////////////////////////////////////////////
			� ///WE GOT A VALID PACKET AND WILL NOW DECODE IT*/
			  switch(messageID)
			  {
							case 1:                 //Mode message
								  mode						= (uint8_t) lastByte;
								  timeOfLastModeMessage		= millis();
								  messagePosition			= 0;
								  messagesReceived++;
								  break;

							case 2:                 //Steering message
								  if(mode != 0)
								  {
									steeringAngleRef = (int8_t) lastByte;
								  }
									messagePosition = 0;
									messagesReceived++;
								  break;

							case 3:                 //Throttle message
								  if(mode != 0)
								  {
									 throttleRef = (uint8_t) lastByte; 
								  }
									 messagePosition = 0;
									 messagesReceived++;
								   break;

							 case 4:   //PID update - Three exactly equal PID parameters handlers. 
									//TODO: Add class that does this
									{
									PID_parametersD[0] = lastByte;

										int i = 1;

										while(i<16 && (micros()-timer<2000))
										{
											if(_HardSerial -> available())
											{ 
												PID_parametersD[i]=_HardSerial -> read();
												i++;
											}                
										}

										//Check if successful read
										if(i==16)
										{
											newPIDD=1;
										}
										messagePosition = 0;
										messagesReceived++;
									}
									break;

							  case 5:                 //PID update
									{
									PID_parametersA[0] = lastByte;
									int i = 1;

									while(i<16 && (micros()-timer<2000))
									{
										if(_HardSerial -> available())
										{ 
											PID_parametersA[i]=_HardSerial -> read();
											i++;
										}                
									}

									if(i==16)
									{
										newPIDA=1;
									}

									messagePosition = 0;
									messagesReceived++;

									}
									break;

							  case 6:                 //PID update
									{
									PID_parametersV[0] = lastByte;

									int i = 1;

									while(i<16 && (micros()-timer<2000))
									{
										if(_HardSerial -> available())
										{ 
											PID_parametersV[i]=_HardSerial -> read();
											i++;
										}                
									}

									if(i==16)
									{
										newPIDV=1;
									}

									messagePosition = 0;
									messagesReceived++;
									}
									break;

							default:
									messagePosition = 0;  //ID not found
									break;
						}
			  break;
      }    
    }

	////////////////////////////////////
	///TIME OUT - Safety feature
    if(millis()-timeOfLastModeMessage>500)    //If the mode has not been updated for 500 ms
    {                                         //it is reset to zero. Safefty function???
      mode = 0;
    }

    if(mode == 0)
    {
      steeringAngleRef = 0;
      throttleRef = 0;
    }
  }

  return messagesReceived;
}
uint8_t MessageHandler::getMode()
{        //Checks if there has been a recent mode message and then returns the mode
  
  if(millis()-timeOfLastModeMessage>500)
  {
    mode				= 0;
    steeringAngleRef	= 0;
    throttleRef			= 0;
  }

  return mode;
}
int8_t  MessageHandler::getSteeringAngleRef()
{
  if(millis()-timeOfLastModeMessage>500)
  {
    mode = 0;
    steeringAngleRef = 0;
    throttleRef = 0;
  }
  return steeringAngleRef;
}
uint8_t MessageHandler::getThrottleRef()
{
  if(millis()-timeOfLastModeMessage>500)
  {
    mode = 0;
    steeringAngleRef = 0;
    throttleRef = 0;
  }
  return throttleRef;
}
void  MessageHandler::sendSteeringAngle(int8_t angle)
{
  _HardSerial -> print("MSA");      //Message start + 'A' for angle
  _HardSerial -> write(angle);
}

uint8_t   MessageHandler::getNewPIDD()
{
  if(newPIDD==1)
  {
    newPIDD=0;
    return 1;
  }
  else
    return 0;
}
uint8_t   MessageHandler::getNewPIDA()
{
  if(newPIDA==1)
  {
    newPIDA=0;
    return 1;
  }
  else
    return 0;
}

uint8_t   MessageHandler::getNewPIDV()
{
  if(newPIDV==1)
  {
    newPIDV=0;
    return 1;
  }
  else
    return 0;
}

byte*     MessageHandler::getPIDD()
{
  return PID_parametersD;
}

byte*     MessageHandler::getPIDA()
{
  return PID_parametersA;
}
byte*     MessageHandler::getPIDV()
{
  return PID_parametersV;
}







