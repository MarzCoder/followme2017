// settings.h

#ifndef _SETTINGS_h
#define _SETTINGS_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include "globals.h"

#ifdef LEADER_BOAT
#define GPSserialPort Serial2
#endif 

#ifdef RESCUE_RUNNER
#define GPSserialPort Serial1
#endif 

namespace wayPointParams
{
	/*The trackingSystem have to be at least this far from each other*/
	const float distanceFilter = 3;

	/*Dont allow more than this number of trackingSystem*/
	const uint8_t maxNumber = 100; 

}

namespace baud
{
	const uint32_t GPS = 115200; 
	//const uint32_t GPS = 19200;
	const uint32_t UI = 115200; 
}

namespace pin
{
	namespace radio
	{
		const uint8_t CE = 9;
		const uint8_t CSN = 10; 
		
	}

}
 

#endif

