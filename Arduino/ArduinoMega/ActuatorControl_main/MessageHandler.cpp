#include "MessageHandler.h"

MessageHandler::MessageHandler(){
  mode = 0;
  steeringAngleRef = 0;
  throttleRef = 0;
  messagePosition = 0;
  messageID = 0;
  lastByte = 0;
  timeOfLastModeMessage = 0;
  _HardSerial = &Serial2;
  _HardSerial -> begin(19200);
}

MessageHandler::MessageHandler(HardwareSerial* hwSerial){
  mode = 0;
  steeringAngleRef = 0;
  throttleRef = 0;
  messagePosition = 0;
  messageID = 0;
  lastByte = 0;
  timeOfLastModeMessage = 0;
  _HardSerial = hwSerial;
  _HardSerial->begin(19200);
  
}

int MessageHandler::checkMessages(uint16_t microSeconds){  //check for messages for x microseconds
  uint32_t timer = micros();
  byte lastByte;
  int messagesReceived = 0;
  while(micros()-timer<microSeconds)
  {
    if(_HardSerial -> available())
    {
      lastByte = _HardSerial -> read();
      switch(messagePosition){
        case 0:
          if(lastByte==0x4D)  //0x4D=M
            messagePosition = 1;    //First part of message start detected.
          break;
        case 1:
          if(lastByte==0x53)  //0x53=S
          {
            messagePosition = 2;    //Second part of message start detected.
          }
          else
          {
            messagePosition = 0;    
          }
          break;
        case 2:
          messageID = lastByte;
          messagePosition = 3;
          break;
        default:
          switch(messageID){
            case 1:                 //Mode 
              mode = (uint8_t) lastByte;
              timeOfLastModeMessage = millis();
              messagePosition = 0;
              messagesReceived++;
              break;
            case 2:                 //Steering message
              if(mode != 0)
              {
                steeringAngleRef = (int8_t) -lastByte;
              }
                messagePosition = 0;
                messagesReceived++;
              break;
            case 3:                 //Throttle message
              if(mode != 0)
              {
                 throttleRef = (uint8_t) lastByte; 
              }
                 messagePosition = 0;
                 messagesReceived++;
               break;
            default:
              messagePosition = 0;  //ID not found
              break;
          }
          break;
      }    
    }
    if(millis()-timeOfLastModeMessage>500)    //If the mode has not been updated for 500 ms
    {                                         //it is reset to zero.
      mode = 0;
    }
    if(mode == 0)
    {
      steeringAngleRef = 0;
      throttleRef = 0;
    }
  }
  return messagesReceived;
}
uint8_t MessageHandler::getMode(){        //Checks if there has been a recent mode message and then returns the mode
  if(millis()-timeOfLastModeMessage>500)
  {
    mode = 0;
    steeringAngleRef = 0;
    throttleRef = 0;
  }
  return mode;
}
int8_t  MessageHandler::getSteeringAngleRef(){
  if(millis()-timeOfLastModeMessage>500)
  {
    mode = 0;
    steeringAngleRef = 0;
    throttleRef = 0;
  }
  return steeringAngleRef;
}
uint8_t MessageHandler::getThrottleRef(){
  if(millis()-timeOfLastModeMessage>500)
  {
    mode = 0;
    steeringAngleRef = 0;
    throttleRef = 0;
  }
  return throttleRef;
}
void  MessageHandler::sendSteeringAngle(int8_t angle){
  _HardSerial -> print("MSA");      //Message start + 'A' for angle
  _HardSerial -> write(angle);
}

