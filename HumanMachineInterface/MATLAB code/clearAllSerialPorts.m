currentPorts = instrfindall;  

if( ~isempty(currentPorts)) 
    fclose(currentPorts); 
    delete(currentPorts); 
end

clear currentPorts; 