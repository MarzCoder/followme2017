%% Communication tests 

% Used to see if it possible just to send "MS" and get an ok back from the
% arduino
%
%


close all; 

Fread = 10; %How often we should read the LB 
Tloop = 1/Fread; 

%Clear all previous connection to avoid conflicts
clearAllSerialPorts; 

%Check some where else to which COM port the arduino is connected 
Arduino = serial('COM15','BaudRate',115200);

%Should always be contious, else there can be a loss of data
Arduino.ReadAsyncMode = 'continuous';

%Use an 1 mb buffer 
Arduino.InputBufferSize = 1000000; 

%Connect_to_arduino

fopen(Arduino);

flushinput(Arduino);  

tic; 

for i=1:1000
   
    fwrite(Arduino,['M','S']);
    
    fgets(Arduino)
   
    while(toc< Tloop)

    end

    tic
end
    
    






