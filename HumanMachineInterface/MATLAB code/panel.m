function panel(Arduino, readFreq)
   % Uses a serial port reference called arduino, and the readFreq
   % argument, eg. at what frequency we should poll data
   
%% Poll LB arduino at this interval 
Tread = 1/readFreq; 
   
global LBPosData RRPosData RRHeadData RRContData StatusData;
LBPosData = [];
RRPosData = [];
RRHeadData = [];
RRContData = [];
StatusData = [];

%LBPosData = LBPosWE, LBPosNS, LBVelWE, LBVelNS, time
%RRPosData = RRPosWE, RRPosNS, RRVelWE, RRVelNS, time
%RRHeadData = RRHead, time
%RRContData = RRMode, RRThrottle, RRRudder, time
%StatusData = RadioStatus, time

%     headingRRBias=-0.1361;
%     headingRRBias=0.13;
headingRRBias=0;
posStart=zeros(2,1);
posEnd=zeros(2,1);
angleStart=0;
    
%% Read PID parameters from external file 
for i=1:1
    fileID = fopen('PID_parameters.txt','r');
    PID_parameters = fscanf(fileID,'%f %f %f %f\n',[4 3])';
    fclose(fileID);

    %Angular velocity PID
    avKp=PID_parameters(1,1);
    avKi=PID_parameters(1,2);
    avKd=PID_parameters(1,3);    
    avTf=PID_parameters(1,4);

    %Angle PID
    aKp=PID_parameters(2,1);
    aKi=PID_parameters(2,2);
    aKd=PID_parameters(2,3);  
    aTf=PID_parameters(2,4);

    %Throttle PID
    tKp=PID_parameters(3,1);
    tKi=PID_parameters(3,2);
    tKd=PID_parameters(3,3);
    tTf=PID_parameters(3,4);
end

%% Generate user 
for i=1:1
    control_panel =figure('Visible','off');
    panel1 = uipanel('FontSize',12,...
             'BackgroundColor','white',...
             'Position',[0 0 0.3 1]);

     panel2 = uipanel('FontSize',12,...
             'BackgroundColor','white',...
             'Position',[0.3 0 0.7 1]);
    herp=2;

    btn_mode0 = uicontrol('Parent',panel1,'Style', 'pushbutton', 'String', 'Mode 0',...
            'Units','normalized','Position', [0 0.95 .25 .05],...
            'Callback', @setMode0);  
    btn_mode1 = uicontrol('Parent',panel1,'Style', 'pushbutton', 'String', 'Mode 1',...
        'Units','normalized','Position', [0.25 0.95 .25 .05],...
        'Callback', @setMode1);  
    btn_mode2 = uicontrol('Parent',panel1,'Style', 'pushbutton', 'String', 'Mode 2',...
        'Units','normalized','Position', [0.50 0.95 .25 .05],...
        'Callback', @setMode2); 
    btn_mode3 = uicontrol('Parent',panel1,'Style', 'pushbutton', 'String', 'Mode 3',...
        'Units','normalized','Position', [0.75 0.95 .25 .05],...
        'Callback', @setMode3); 
    modetxt = uicontrol('Parent',panel1,'Style','text',...
        'Units','normalized','Position',[0 0.9 0.33 .05],...
        'String','Mode: 0');
    modeRRtxt = uicontrol('Parent',panel1,'Style','text',...
        'Units','normalized','Position',[0.33 0.9 0.33 .05],...
        'String','ModeRR: 0');

    txt2 = uicontrol('Parent',panel1,'Style','text',...
        'Units','normalized','Position',[0 0.85 1 .05],...
        'String','Distance PID');  
    txtavKp =uicontrol('Parent',panel1,'Style', 'edit',...
        'Units','normalized','Position',[0 0.80 0.25 .05],...
        'String','Kp');
    txtavKi =uicontrol('Parent',panel1,'Style', 'edit',...
        'Units','normalized','Position',[0.25 0.80 0.25 .05],...
        'String','Ki');
    txtavKd =uicontrol('Parent',panel1,'Style', 'edit',...
        'Units','normalized','Position',[0.5 0.80 0.25 .05],...
        'String','Kd');
    txtavTf =uicontrol('Parent',panel1,'Style', 'edit',...
        'Units','normalized','Position',[0.75 0.80 0.25 .05],...
        'String','Tf');
    avKpedit = uicontrol('Parent',panel1,'Style', 'edit',...
        'Units','normalized','Position',[0 0.75 0.25 .05],...
        'String',num2str(avKp),'Callback', @avKpedit_cb);
    avKiedit = uicontrol('Parent',panel1,'Style', 'edit',...
        'Units','normalized','Position',[0.25 0.75 0.25 .05],...
        'String',num2str(avKi),'Callback', @avKiedit_cb);
    avKdedit = uicontrol('Parent',panel1,'Style', 'edit',...
        'Units','normalized','Position',[0.5 0.75 0.25 .05],...
        'String',num2str(avKd),'Callback', @avKdedit_cb);
    avTfedit = uicontrol('Parent',panel1,'Style', 'edit',...
        'Units','normalized','Position',[0.75 0.75 0.25 .05],...
        'String',num2str(avTf),'Callback', @avTfedit_cb);

    txt3 = uicontrol('Parent',panel1,'Style','text',...
        'Units','normalized','Position',[0 0.7 1 .05],...
        'String','Angle PID');
    txtaKp =uicontrol('Parent',panel1,'Style', 'edit',...
        'Units','normalized','Position',[0 0.65 0.25 .05],...
        'String','Kp');
    txtaKi =uicontrol('Parent',panel1,'Style', 'edit',...
        'Units','normalized','Position',[0.25 0.65 0.25 .05],...
        'String','Ki');
    txtaKd =uicontrol('Parent',panel1,'Style', 'edit',...
        'Units','normalized','Position',[0.5 0.65 0.25 .05],...
        'String','Kd');
    txtaTf =uicontrol('Parent',panel1,'Style', 'edit',...
        'Units','normalized','Position',[0.75 0.65 0.25 .05],...
        'String','Tf');
    aKpedit = uicontrol('Parent',panel1,'Style', 'edit',...
        'Units','normalized','Position',[0 0.60 0.25 .05],...
        'String',num2str(aKp),'Callback', @aKpedit_cb);
    aKiedit = uicontrol('Parent',panel1,'Style', 'edit',...
        'Units','normalized','Position',[0.25 0.60 0.25 .05],...
        'String',num2str(aKi),'Callback', @aKiedit_cb);
    aKdedit = uicontrol('Parent',panel1,'Style', 'edit',...
        'Units','normalized','Position',[0.5 0.60 0.25 .05],...
        'String',num2str(aKd),'Callback', @aKdedit_cb);
    aTfedit = uicontrol('Parent',panel1,'Style', 'edit',...
        'Units','normalized','Position',[0.75 0.60 0.25 .05],...
        'String',num2str(aTf),'Callback', @aTfedit_cb);

    txt4 = uicontrol('Parent',panel1,'Style','text',...
        'Units','normalized','Position',[0 0.55 1 .05],...
        'String','Velocity PID');
    txttKp =uicontrol('Parent',panel1,'Style', 'edit',...
        'Units','normalized','Position',[0 0.50 0.25 .05],...
        'String','Kp');
    txttKi =uicontrol('Parent',panel1,'Style', 'edit',...
        'Units','normalized','Position',[0.25 0.50 0.25 .05],...
        'String','Ki');
    txttKd =uicontrol('Parent',panel1,'Style', 'edit',...
        'Units','normalized','Position',[0.5 0.50 0.25 .05],...
        'String','Kd');
    txttTf =uicontrol('Parent',panel1,'Style', 'edit',...
        'Units','normalized','Position',[0.75 0.50 0.25 .05],...
        'String','Tf');
    tKpedit = uicontrol('Parent',panel1,'Style', 'edit',...
        'Units','normalized','Position',[0 0.45 0.25 .05],...
        'String',num2str(tKp),'Callback', @tKpedit_cb);
    tKiedit = uicontrol('Parent',panel1,'Style', 'edit',...
        'Units','normalized','Position',[0.25 0.45 0.25 .05],...
        'String',num2str(tKi),'Callback', @tKiedit_cb);
    tKdedit = uicontrol('Parent',panel1,'Style', 'edit',...
        'Units','normalized','Position',[0.5 0.45 0.25 .05],...
        'String',num2str(tKd),'Callback', @tKdedit_cb);
    tTfedit = uicontrol('Parent',panel1,'Style', 'edit',...
        'Units','normalized','Position',[0.75 0.45 0.25 .05],...
        'String',num2str(tTf),'Callback', @tTfedit_cb);

    sendbtn = uicontrol('Style', 'pushbutton', 'String', 'Send',...
            'Position', [20 20 50 20],...
            'Callback', @send_cb);
    savebtn = uicontrol('Style', 'pushbutton', 'String', 'Save',...
            'Position', [60 20 50 20],...
            'Callback', @save_cb);
    startmagbiasbtn = uicontrol('Style', 'pushbutton', 'String', 'Start Calib',...
            'Position', [110 20 50 20],...
            'Callback', @startmagbias_cb);
    stopmagbiasbtn = uicontrol('Style', 'pushbutton', 'String', 'Stop Calib',...
            'Position', [160 20 50 20],...
            'Callback', @stopmagbias_cb);
    startCompassTestbtn = uicontrol('Style', 'pushbutton', 'String', 'Start compass read',...
        'Position', [20 60 120 20],...
        'Callback', @compass_cb);
    control_panel.Visible = 'on';


    subplot(4,2,1,'Parent',panel2,'Units','normalized',...
        'Position',[.05 .833 .9 .12]);
    rudderplot_y=zeros(1,200);
    figure_handle_Rudder=plot(rudderplot_y);
    title('Rudder angle');
    ylim([-100 100]);


    subplot(4,2,3,'Parent',panel2,'Units','normalized',...
        'Position',[.05 .666 .9 .12]);
    throttleplot_y=zeros(1,200);
    figure_handle_Throttle=plot(throttleplot_y);
    title('Throttle');
    ylim([0 100]);

    subplot(4,2,5,'Parent',panel2,'Units','normalized',...
        'Position',[.05 .5 .9 .12]);
    velocityplot_y=zeros(1,200);
    figure_handle_Velocity=plot(velocityplot_y);
    title('Velocity m/s');
    ylim([0 20]);

    subplot(4,2,7,'Parent',panel2,'Units','normalized',...
        'Position',[.05 .05 .4 .36]);
    scale=2;
    color='b';
    axisLength = 100;
    temp=linspace(pi/4,2*pi/3,10);
    hull = [[cos(temp) fliplr(cos(temp))]-(cos(pi/4)+cos(2*pi/3))/2;
            sin(temp)-sin(temp(1)) -fliplr(sin(temp))+sin(temp(1))];
                    % The hull is constructed from circular arcs.
    hull=hull/max(hull(1,:))*1.8; % The length of the boat is 3.6.
    % R=[cos(psi), -sin(psi);     % Rotation matrix
    %        sin(psi),  cos(psi)];    %
    boat=1*hull*scale;          % Rotate and scale 
    boatx=boat(1,:);
    boaty=boat(2,:);
    figure_handle=plot(boatx,boaty,'Color',color);
    hold on
    axis equal;
    axisRange=[-axisLength axisLength -axisLength axisLength]/2;
    axis(axisRange);

    % RescueRunner
    scaleRR=1;
    colorRR='r';
    boat=1*hull*scaleRR;          % Rotate and scale 
    boatx=boat(1,:);
    boaty=boat(2,:);
    figure_handleRR=plot(boatx,boaty,'Color',colorRR);

    subplot(4,2,8,'Parent',panel2,'Units','normalized',...
        'Position',[.55 .05 .4 .36]);
    radioplot_y=20*ones(1,100);
    figure_handle_radio=plot(radioplot_y);
    title('Connection status');
    ylim([0 45]);

%      figure
end
    
% if(~exist('joy','var'))
%     joy = vrjoystick(1);
% end

%% Initalization of communcation and plots 
 flushinput(Arduino);   
 
 StartCompass=0;
 GPSVec=[];
 HeadingVec=[];

modeRR=0;
posLB=[12 57.7]'
velLB=[0 0]';
posRR=[12 57.7]'
velRR=[0 0]';
headingLB = 0;
headingRR = 0;
mode=0;
axisLength = 80;
axisRange=[-axisLength axisLength -axisLength axisLength]/2;

lat2m = 111000*10^-7;
lon2m = 59000*10^-7;

plot_divider = 0;
send_divider = 1;
tic;

  %% Main loop that reads data and plots it 
while true

    %% Draw some stuff here
    if(plot_divider==0)
        plot_divider=4;
        set(figure_handle_Rudder,'YData',rudderplot_y);
        set(figure_handle_Throttle,'YData',throttleplot_y);
        set(figure_handle_Velocity,'YData',velocityplot_y);
        set(figure_handle_radio,'YData',radioplot_y);
%             R=[cos(headingLB), -sin(headingLB);     % Rotation matrix
%            sin(headingLB),  cos(headingLB)];    %
%         boat=R*hull*scale+repmat([posLB(1)./10^7;posLB(2)./10^7],1,20); 
%             Draw(figure_handle,posLB/10^7,headingLB,scale); %Draw lead boat
        Draw(figure_handle,[0 0],headingLB+pi/2,scale); %Draw lead boat
        Draw(figure_handleRR,(posRR-posLB).*[lon2m lat2m]',headingRR+pi/2+headingRRBias,scaleRR);  %Draw RR
        %Resize_axes(posLB/10^7,axisRange,axisLength);

        drawnow;
    else
        plot_divider=plot_divider-1;
    end

    %% The receiver wants the mode regularly, will else go to default off mode
    if(send_divider==0)
        send_divider=4;

        fwrite(Arduino,['M','S',1,mode]);           %MS: message start, 1: message type mode, mode: mode type
%             if(mode==2||mode==1||mode==3)
       
         %What does even mode 2 mean? Retarded, change to enums
        if(mode==2)
            steering_ref = int8(Read_steering(joy));
            throttle_ref = uint8(Read_throttle(joy));
            
            fwrite(Arduino,['M','S',2]);              % Message type steering reference 
            fwrite(Arduino,steering_ref,'int8');
            
            fwrite(Arduino,['M','S',3,throttle_ref]); %Message type throttle refernce 
        end
    else
        send_divider=send_divider-1;
    end

    %% This is where the Arduino is read. Read a byte and to different things
    %% according to the byte read

    if(Arduino.BytesAvailable > 0)

                    temp=fread(Arduino,1,'char');
                    
                    %TODO: Change to switch/case for cleaner syntax??
                    if(temp == 'c') 
                        modeRR=fread(Arduino,1,'uint8');
                        set(modeRRtxt,'String',['ModeRR: ' num2str(modeRR)]);
                        rudderRR=fread(Arduino,1,'int8');
                        throttleRR=fread(Arduino,1,'uint8');
                        rudderplot_y=[rudderplot_y(2:end) rudderRR];
                        throttleplot_y=[throttleplot_y(2:end) throttleRR];
                        RRContData = [RRContData;modeRR,throttleRR,rudderRR,datenum(datestr(now,'HHMMSSFFF'),'HHMMSSFFF')];
                    elseif(temp=='E')
                        if(fread(Arduino,1,'uint8')==1)
                            fclose(Arduino);
                            error('I2C Error')
                        end
                    elseif(temp == 'p')
                        posLB=fread(Arduino,2,'int32');
                        velLB=fread(Arduino,2,'int32')/1000.*[1;-1];
                        lastGPSLB=fread(Arduino,1,'uint32');
                        posRR=fread(Arduino,2,'int32');
                        velRR=fread(Arduino,2,'int32')/1000;
                        velocityplot_y=[velocityplot_y(2:end) norm(velRR)];
                        lastGPSRR=fread(Arduino,1,'uint32');
                        LBPosData=[LBPosData;posLB',velLB',datenum(datestr(now,'HHMMSSFFF'),'HHMMSSFFF')];
                        RRPosData=[RRPosData;posRR',velRR',datenum(datestr(now,'HHMMSSFFF'),'HHMMSSFFF')];
                        %posLB=[0 0]';
                        %posRR=[0 1200]';
                        if(StartCompass>0)
                            GPSVec=[GPSVec;((posLB-posRR).*[lon2m lat2m]')'];
                        end
                    elseif(temp == 'h')
                        qLB = fread(Arduino,4,'int32');
                        qRR = fread(Arduino,4,'int32');
                        derp=[1;0];
                        Q2=Qq(normalizeX(qLB));
                        herp = Q2(1:2,1:2)*derp;
                        headingLB = atan2(herp(2),herp(1));
                        Q2=Qq(normalizeX(qRR));
                        herp = Q2(1:2,1:2)*derp;
                        headingRR = atan2(herp(2),herp(1));
                        RRHeadData=[RRHeadData;headingRR,datenum(datestr(now,'HHMMSSFFF'),'HHMMSSFFF')];
                        if(StartCompass>0)
                            HeadingVec=[HeadingVec;headingRR];
                        end
                    elseif(temp == 'r')
                        radioStatus = fread(Arduino,1,'uint8');
                        radioplot_y=[radioplot_y(2:end) radioStatus];
                        StatusData = [StatusData;radioStatus,datenum(datestr(now,'HHMMSSFFF'),'HHMMSSFFF')];
                    elseif(temp == 'M')
                        magCorrection = fread(Arduino,1,'float')
                    end
 end

%% Start to read the compass 
if(StartCompass>0)
    StartCompass=StartCompass-1;
    if(StartCompass==0)
        GPSmean=mean(GPSVec)
        GPSHeading = atan2(-GPSmean(1),GPSmean(2))
        CompassHeading = mean(HeadingVec)
        fileID = fopen(['compass' datestr(now,'mmddHHMMSS') '.txt'],'w');
    fprintf(fileID,'GPSmean %f %f\n',GPSmean(1),GPSmean(2));
    fprintf(fileID,'GPSHeading %f\n',GPSHeading);
    fprintf(fileID,'CompassHeading %f\n',CompassHeading);
    fclose(fileID);
    end
end

%% Delay to get constant loop time
while(toc< Tread)

end

tic
end
%END OF WHILE LOOP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Looks like function chanhing the GUI, connected to the buttons
    function setMode0(source,callbackdata)
        mode=0;
        set(modetxt,'String','Mode: 0');
    end
    function setMode1(source,callbackdata)
        mode=1;
        set(modetxt,'String','Mode: 1');
    end
    function setMode2(source,callbackdata)
        mode=2;
        set(modetxt,'String','Mode: 2');
    end
    function setMode3(source,callbackdata)
        mode=3;
        set(modetxt,'String','Mode: 3');
    end
    function avKpedit_cb(source,callbackdata)
        avKp = str2double(get(source,'string'))
    end
    function avKiedit_cb(source,callbackdata)
        avKi = str2double(get(source,'string'))
    end
    function avKdedit_cb(source,callbackdata)
        avKd = str2double(get(source,'string'))
    end
    function avTfedit_cb(source,callbackdata)
        avTf = str2double(get(source,'string'))
    end
    function aKpedit_cb(source,callbackdata)
        aKp = str2double(get(source,'string'))
    %         set(txt2,'String',num2str(input));
    end
    function aKiedit_cb(source,callbackdata)
        aKi = str2double(get(source,'string'))
    end
    function aKdedit_cb(source,callbackdata)
        aKd = str2double(get(source,'string'))
    end
    function aTfedit_cb(source,callbackdata)
        aTf = str2double(get(source,'string'))
    end
    function tKpedit_cb(source,callbackdata)
        tKp = str2double(get(source,'string'))
    %         set(txt2,'String',num2str(input));
    end
    function tKiedit_cb(source,callbackdata)
        tKi = str2double(get(source,'string'))
    end
    function tKdedit_cb(source,callbackdata)
        tKd = str2double(get(source,'string'))
    end
    function tTfedit_cb(source,callbackdata)
        tTf = str2double(get(source,'string'))
    end
    function save_cb(source,callbackdata)
        fileID = fopen(['PID_parameters' datestr(now,'mmddHHMM') '.txt'],'w');
        fprintf(fileID,'%f %f %f %f\n',avKp,avKi,avKd,avTf);
        fprintf(fileID,'%f %f %f %f\n',aKp,aKi,aKd,aTf);
        fprintf(fileID,'%f %f %f %f\n',tKp,tKi,tKd,tTf);
        fclose(fileID);
    end   
    function send_cb(source,callbackdata)
        fwrite(Arduino,['M','S',4]);
        fwrite(Arduino,[avKp avKi avKd avTf],'single')
        fwrite(Arduino,['M','S',5]);
        fwrite(Arduino,[aKp aKi aKd aTf],'single')
        fwrite(Arduino,['M','S',6]);
        fwrite(Arduino,[tKp tKi tKd tTf],'single')
    end

    function startmagbias_cb(source, callbackdata)
     posStart=posRR
     angleStart=headingRR+pi/2     
    end

    function stopmagbias_cb(source, callbackdata)
     posStop=posRR
     posLB
     tempBias = (posStop-posStart).*[lon2m lat2m]'
     temp2Bias= atan2(tempBias(2),tempBias(1))
     
     headingRRBias=temp2Bias-angleStart
    end
%     function throttle_ref = Read_throttle(joyObject)
%         [axes, buttons, ~] = read(joyObject);
%         if(axes(3)>0.95)
%             axes(3)=1;
%         end
% 
%         throttle_ref = -(axes(3)-1)/2*100;
%     end
    function compass_cb(source, callbackdata)
        StartCompass=500;
        GPSVec=[];
        HeadingVec=[];
        Arduino.BytesAvailable
        
    end
    function []=Draw(figure,x,psi,scale)
%     persistent hull;    % Make the variable hull persistent so that it does
                            % not have to be recomputed in each function call.

%         if(isempty(hull))   % Only true for the first call, or when the 'clear'
                            % command has been issued.
%             temp=linspace(pi/4,2*pi/3,10);
%             hull = [[cos(temp) fliplr(cos(temp))]-(cos(pi/4)+cos(2*pi/3))/2;
%                     sin(temp)-sin(temp(1)) -fliplr(sin(temp))+sin(temp(1))];
%                             % The hull is constructed from circular arcs.
%             hull=hull/max(hull(1,:))*1.8; % The length of the boat is 3.6.
%         end
        R=[cos(psi), -sin(psi);     % Rotation matrix
           sin(psi),  cos(psi)];    %
        boat=R*hull*scale+repmat([x(1);x(2)],1,20);          % Rotate and scale
        set(figure,'XData',boat(1,:),'YData',boat(2,:));
        %drawnow;
    end

end