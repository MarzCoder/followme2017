% This scrips can be used to find the bias and scale error of the
% magnetometer. The measurements are read by the arduino and forwarded
% to MATLAB over USB. The option "dataToSend +=SEND_MAG;" in the setup code
% must be uncommented.
%
% The measurements are plotted in a running graph and the latest min and
% max value are printed in the terminal. When the max and min for all axes
% have been found the second section of the script can be run. This prints
% a copy-pasteable section of code that can be inserted into the arduino 
% code.



Open_arduino;
pause(1.5)

magBias =[0;0;0];
magScale = [1;1;1];
i=0;
j=0;
magmax=zeros(1,3);
magmin=zeros(1,3);
magx=zeros(1,100);
magy=zeros(1,100);
magz=zeros(1,100);
maglastx=zeros(1,100);
maglasty=zeros(1,100);
maglastz=zeros(1,100);
magv=zeros(3,3);
lastmagv = [0;0;0];
clf
%figure
hold on
maghx=plot([0],[0],'b','YDataSource', 'magx', 'XDataSource', 'sample');
maghy=plot([0],[0],'r','YDataSource', 'magy', 'XDataSource', 'sample');
maghz=plot([0],[0],'g','YDataSource', 'magz', 'XDataSource', 'sample');

N=10000;
mag_calibration_data = zeros(3,N);



newq=0;
newAcc=0;
newGyr=0;
newMag=0;
newGPS=0;
newP=0;
newDelay=0;

qdot = [0;0;0;0];
lastqdot=qdot;

k=1;
while 1
    buffer = Due.BytesAvailable;
    lastByte = fread(Due,1,'char');
    if(lastByte=='q')
            q = fread(Due,4,'int32');
            newq=1;
        elseif(lastByte=='g')
            gyrData = fread(Due,3,'int16');
            gyrv = gyrData/16.4/180*pi;
            newGyr=1;
        elseif(lastByte=='a')
            accData = fread(Due,3,'int16');
            accv = 9.82*accData/4096;
            newAcc=1;
        elseif(lastByte=='m')
            magData = fread(Due,3,'int16');
            magv=magData;
            newMag=1;
        elseif(lastByte=='E')
            if(fread(Due,1,'uint8')==1)
                fclose(Due);
                error('I2C Error')
            end
    end

    if(newMag)
        newMag=0;
        mag_calibration_data(:,k) = magData';
        k=k+1;
        magx=[magx(2:end) magv(1)];
        magy=[magy(2:end) magv(2)];
        magz=[magz(2:end) magv(3)];
        if(abs(magv(1)-magx(end-1))<20)
            if(magv(1)>magmax(1))
                magmax(1)=magv(1)
            end
            if(magv(1)<magmin(1))
                magmin(1)=magv(1)
            end
        end
        if(abs(magv(2)-magy(end-1))<20)
            if(magv(2)>magmax(2))
                magmax(2)=magv(2)
            end
            if(magv(2)<magmin(2))
                magmin(2)=magv(2)
            end
        end
        if(abs(magv(3)-magz(end-1))<20)
            if(magv(3)>magmax(3))
                magmax(3)=magv(3)
            end
            if(magv(3)<magmin(3))
                magmin(3)=magv(3)
            end
        end
        sample=i-99:i;
        if(j==0)
            set(maghx,'Xdata',sample,'YData',magx);
            set(maghy,'Xdata',sample,'YData',magy);
            set(maghz,'Xdata',sample,'YData',magz);

            xlim([i-99 i])
            j=20;
            drawnow
        end
        j=j-1;
        i=i+1;

    end        
end
%%
magBias2 = (magmax+magmin)/2;
magScale2 = (magmax-magmin)/2;
magMean2 = mean(magScale2);
magScale3 = magScale2/magMean2;
'scale =';
int32(1./magScale3*2^15);
magScale4=1./magScale3*2^15;
fprintf('int32_t magBias[3]={%.0f,%.0f,%.0f}; //scale 2^0\n',magBias2(1),magBias2(2),magBias2(3))
fprintf('int32_t magScale[3]={%.0f,%.0f,%.0f}; //scale 2^15\n',magScale4(1),magScale4(2),magScale4(3))

