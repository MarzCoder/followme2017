%%
% The data collected during the tests was saved on the following format:
%
% LBGPS = [latitude (deg), longitude (deg), velocity north, velocity west(!), time (seconds)]
% RRGPS = [latitude (deg), longitude (deg), velocity north, velocity west(!), time (seconds)]
% RRHeading = [heading (rad), time (seconds)]
% RRControl = [mode [0,1,2], rudder [-100:100], throttle [0:100], time (seconds)] (only updated in autonomous mode)
%
% Where the "time" is the time the data was read in MATLAB (i.e. not
% exactly when it was sampled by the arduino), but otherwise synced.
%
% Below is a script that plots the two moving boats.

close all
clear all; 
load 'Manual_driving'
axisLength = 100;



scale=3.2222;
color='b';
figure_handle=plot(0,0,'Color',color);
hold on
axis equal;
axisRange=[-axisLength axisLength -axisLength axisLength]/2;
 axis(axisRange);
 xlabel('[m]');
 ylabel('[m]');

% RescueRunner
scaleRR=1.111;
colorRR='r';
figure_handleRR=plot(0,0,'Color',colorRR);

lat2m = 111371.77003417036;
lon2m = 59675.85161707114;

startTime=0;
playback_speed = 4;

step_size=1/200/playback_speed;
delay_compensation=0.992;

tick=tic;

for i=1*200+startTime*200:RRGPS(end,5)*200
    LBGPSIndex=find(LBGPS(:,5)<=i/200,1,'last');
    RRGPSIndex=find(RRGPS(:,5)<=i/200,1,'last');
    RRHeadingIndex=find(RRHeading(:,2)<=i/200,1,'last');
    Draw(figure_handle,fliplr((LBGPS(startTime+LBGPSIndex,1:2)-LBGPS(1,1:2))).*10^-7.*[lon2m lat2m],-atan2(LBGPS(startTime+LBGPSIndex+50,3),-LBGPS(startTime+LBGPSIndex+50,4))+pi,scale); %Draw lead boat
    Draw(figure_handleRR,fliplr(RRGPS(startTime+RRGPSIndex,1:2)-LBGPS(1,1:2)).*10^-7.*[lon2m lat2m],RRHeading(startTime+RRHeadingIndex,1)+pi/2,scaleRR);
    axisRange = Resize_axes(fliplr(LBGPS(startTime+LBGPSIndex,1:2)-LBGPS(1,1:2)).*10^-7.*[lon2m lat2m],axisRange,axisLength);
    if(~rem(i,5))
        drawnow;
    end

    Seconds = i/200+1

    delay((step_size-toc(tick))*delay_compensation);
    delay_compensation=delay_compensation+(step_size-toc(tick))*0.05;
    tick=tic;
end