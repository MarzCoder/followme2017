function []=Draw(figure,x,psi,scale)
     persistent hull;    % Make the variable hull persistent so that it does
                            % not have to be recomputed in each function call.

        if(isempty(hull))   % Only true for the first call, or when the 'clear'
%                             command has been issued.
            temp=linspace(pi/4,2*pi/3,10);
            hull = [[cos(temp) fliplr(cos(temp))]-(cos(pi/4)+cos(2*pi/3))/2;
                    sin(temp)-sin(temp(1)) -fliplr(sin(temp))+sin(temp(1))];
                            % The hull is constructed from circular arcs.
            hull=hull/max(hull(1,:))*1.8; % The length of the boat is 3.6.
        end
        R=[cos(psi), -sin(psi);     % Rotation matrix
           sin(psi),  cos(psi)];    %
        boat=R*hull*scale+repmat([x(1);x(2)],1,20);          % Rotate and scale
        set(figure,'XData',boat(1,:),'YData',boat(2,:));
        %drawnow;
    end