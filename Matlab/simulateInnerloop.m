clear all;
close all; 

%Simulation of the velocity and angle controllers only 

close all;
clear all; 

%% Parameters 

parameters; %Parameters of hull, engines etc

%Noise used in simulink model
% 
% simulatedVariance = struct( 'speed', 1,...
%                             'position', var_xy,...
%                             'angle', (pi/12)^2); 

PID_angle = struct('Kp',    0.05,...
                   'Ki',    0,...
                   'Kd',    0.05,...
                   'Tf',    0.1); 
               
PID_speed = struct( 'Kp', 0.5,...
                    'Ki', 0.01,...
                    'Kd', 0,...
                    'Tf', 0.1); 
                
 %wayPointsParams = struct ('distanceFilter',3); %The waypoints have to be this long from each other
 
 

GPS_T                    =    0.1;                %GPS sampling interval in seconds


%% Run simulation
theta0 = 0; 
sim( 'innerLoop', 200); 

%% Plots 
close all; 

% Throttle and nozzle angle 

figure; 
alpha = alphaBeta.data(:,1); 
beta = alphaBeta.data(:,2); 

h(1) = plot(alphaBeta.time, alpha, 'k'); 
hold on; 
h(2) = plot(alphaBeta.time, ones(size(alpha))*alpha_max, 'k--'); 
plot(alphaBeta.time, -ones(size(alpha))*alpha_max, 'k--');

h(3) = plot(alphaBeta.time, beta, 'r'); 
h(4) = plot(alphaBeta.time, ones(size(alphaBeta.time)), ':r'); 
 plot(alphaBeta.time, zeros(size(alphaBeta.time)), ':r'); 

legend(h,'Nozzle angle','Nozzle angle saturation','Throttle','Throttle saturation'); 

% Plot with control signals 

figure;

%subplot(2,1,1); 
angle       = angleController.data(:,1); 
angleRef    = angleController.data(:,2); 


plot(angleController.time, angle); 
hold on; 
plot(angleController.time, angleRef, 'k--'); 
plot(alphaBeta.time, beta*20*pi, 'r');

legend('Angle', 'Angle reference','Throttle');

%Plot velocity
%subplot(2,1,2); 
figure;
velocity    = velocityController.data(:,1); 
velocityRef = velocityController.data(:,2); 

t_vel = velocityController.time; 

plot(t_vel, velocity); 
hold on; 
plot(t_vel, velocityRef, 'k--');
legend('Velocity','Velocity reference'); 



%Plot forces 
moment = forces.data(:,1); 
thrust = forces.data(:,2); 

figure; 
plot(forces.time, moment,'r');  
hold on; 
plot(forces.time, thrust,'k--');  
legend('Moment', 'Thrust'); 
grid on; 

%Plot engine shaft 
figure; 

shaftPower      = shaft.data(:,1); 
torque          = shaft.data(:,2); 
shaftSpeed      = shaft.data(:,3); 

plot(shaft.time, shaftPower, 'r') ; 

legend('Shaft power'); 

