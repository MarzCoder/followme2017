
%Rescue runner parameters 

%% Engine and turbine 
Pmax     = 75*10^3            %Max shaft power delivered from engine(kW), from manual

RPMmax   = 8000;              %The motor max RPM
omegamax = RPMmax*(2*pi/60);  %Max motor revolutionspeed

OMEGA_0   = 0.8*omegamax;      %Where the motor is thought to deliver max power

KQ       = 0.0002;             %Constant relating propeller speed^3 to shaft power

m_turb   = 5;                   %Assumed mass of turbine wheel + shaft
I_turb   = 0.5*m_turb*0.05^2;   %Turbine and shaft inertia (Inertia for a disc 0.5mr^2

alpha_max = pi/6;               %The nozzle can only move +- 30 degrees 

%% Boat hull 
Vmax    = 20;       %Observed max boat speed
m       = 300;      %Boat mass 
d_cog   = 1.5;      %Distance attach point of thrust - centre of gravity
l       =  4;       %Hull length
w       = 1.5;      %Hull Width
Ts      = 0.01;     %Controller sample interval

maxRotationalVel = 2*pi;      %Assumed max angular vel we could ever achieve 
maxacc    = 3;      %Assumed max acc [m/s^2] of the scooter

F0max    = m*maxacc;          %F = ma
maxThrust = F0max; 
M_max    = F0max*sin(alpha_max)*d_cog; %The max moment the controller can output
maxMoment = M_max; 

Iz  = 1/12*m*(l^2+w^2); %Hull inertia around the jaw axis

CF0      = F0max/(Pmax);      % Used in F0 = CF0*P

%Depending on ship speed
VmaxFree = 100;               %Assumed equlibrium when in "free air" : F = 0 = F0max - Ct*VmaxFree 
Cf       = F0max/VmaxFree;    % F = F0 -Cf*V  our model for thrust

C_drag_front = F0max/Vmax^2-Cf/Vmax; %From equlilibrium F0max = Cf*v + C_drag_front*v^2

C_drag_moment = F0max*sin(alpha_max)/maxRotationalVel^2; %From equilibrium M = Fsin(alpha) = Cd*w^2




