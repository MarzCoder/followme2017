function [Skoter, WP_sist] = Kontroll(Skoter1, Bat, WP_sist)
        %Kontroll Delegerar ut arbetsuppgifter till olika funktioner.
        %   Beh�ver skoterns data, b�tens data och sista waypoint.
        %	Returnerar skoterns sista position och den sista Waypointen.
        %	Skapad av Victor B�ckman, Kandidatarbete SSYX02-15-38 v�ren 2015.
        
        %%% Initiering %%%
        err_vink=zeros(1,50);  
        Skoter2=[];
        Ankring=[];
        
        %%% Begynnelsev�rden %%%
        V1=[Skoter1(3) Skoter1(4) Skoter1(5)];
        a1=0;
        alpha1 = 0;             % Jetvinkel vid start
        Theta1=Skoter1(6);
        
        dt=0.05;                % Tidssteg
        axlar=[-200 200 -200 200];
        
        % F�r banf�ljning %
        bool_plats_j=0; % Om passerad WP
        WP_t=0;         % Tid mellan WP1 och WP2
        WP_t_max=10;    % Maxtid mellan WP1 och WP2 (denna ber�knas igen senare)
        R=5;            % Godk�nd radie om skotern �r framme.
        
        % Regulator %
        err_vink(1)=0;  % M�ste sparas i en vektor f�r att hitta diskont.
        err_hast1=0;
        
        
        %%% Planera en bana %%%
        [Bana, WP_sist] = Interpolation(Skoter1([1,2]), Bat, WP_sist);

        subplot(2,1,1)
        hold on
        Radieplot(Bana,R);
        
        %%% LOS-Banf�ljning %%%
        k=0; % Korrigering vinkelfel
        j=1; % Vilken nuvarande WP
        [Antal_WP, ~] = size(Bana);  % Antal WP
        while j<Antal_WP
            k=k+1;
            pause(0.0000001)
            WP_t=WP_t + dt;     % Ber�kna tid per WP
            
            if bool_plats_j==1 || WP_t > WP_t_max
                j=j+1;
                WP_t=0;
                WP_t_max= abs(norm(Bana(j,[1,2]) - Skoter1([1,2]))/Bana(j,3));
            end
            
            % Data f�r banf�ljning %
            if isempty(Ankring) == 0
                [dS, Phi, bool_plats_j] = Banfoljning(Skoter1([1,2]), Ankring, R);
            else
                [dS, Phi, bool_plats_j] = Banfoljning(Skoter1([1,2]), Bana(j,[1,2]), R);
            end
            
            if k>30     % Sparar endast 30 element (sparar plats)
                k=1;
                err_vink=[];
            end
            err_vink(k+1)=Phi -Theta1; 
            err_vink=unwrap(err_vink);  % Tar bort diskontinuiteter i hela vektorn
            
            alpha2 = Regulator_vinkel(alpha1, err_vink(k), err_vink(k+1), dt, V1);
           
            
            % Gasreglering %
            err_hast2=Bana(j,3) - sqrt(abs(V1(1)^2 + V1(2)^2));
            
            a2 = Regulator_acc(a1, err_hast1, err_hast2, dt, 3);
            a2=a2*(1+WP_t/WP_t_max);   % �kar acc om l�ngre bort 
            
            % Utom r�ckh�ll %
            if norm(dS)> 300 || isempty(Ankring) == 0
                if isempty(Ankring) == 1
                    Ankring=[Skoter1([1,2]), 1; Skoter1([1,2]) + [10 10], 1];
                end
                disp('Skotern f�r l�ngt bort fr�n b�ten, st�r still')
                a2=1;
            end
            
            
            %%% Ny position, hastighet och vinkel %%%
            % H�mtas sen fr�n moduler
            [Skoter2([1,2]), V2, Theta2] = Skotermodell(Skoter1([1,2]), V1, Theta1, dt, a2, alpha2);
            
            %%% Plottar %%%
            % Skoterns position %
            subplot(2,1,1)
            grid on
            axis(axlar)
            title('Vattenskoterns positioner och Waypoints')
            plot(Skoter2(1),Skoter2(2),'.')
            hold on
            
            % Skoterns riktning och v�rden %
            subplot(2,1,2)
            title('Vattenskoterns lokala orientering')
            Skoterplot(alpha2, Theta2, a2, V2)
            hold on
            
            %fileID = fopen('spara_vink.txt');
            %Spara_vink=[Theta2, Phi, err_vink(k+1), alpha2];
            %formatSpec = '%.5f\n';
            %fprintf(fileID, formatSpec, Spara_vink);
            
            Spara=[Theta2, Phi, err_vink(k+1), alpha2, V2(1), Bana(j,3), err_hast2, a2, norm(abs(dS)), dt, bool_plats_j];
            %dlmwrite('spara.txt',Spara,'-append','delimiter','\t','roffset',1,'newline','pc')
            dlmwrite('spara.txt',Spara,'-append','delimiter','\t','newline','pc')

            %Spara_hast=[V2, Bana(j,3), a2, dS, dt];
            %dlmwrite('spara_vink.txt',Spara_hast,'-append','delimiter','\t','roffset',1,'newline','pc')
            
            V1=V2;
            a1=a2;
            alpha1=alpha2;
            Skoter1([1,2]) = Skoter2([1,2]);
            Theta1=Theta2;
            err_hast1=err_hast2;
            
        end     %end while
        
        % Returnera %
        Skoter=[Skoter2([1,2]), V2, Theta2];
            
    end 


