function F = Fjet(riktning,a,alpha)
        %Fjet Kraften som jeten ut�var p� b�ten.
        %   Olika kraft beroende p� vilken riktning som �nskas.
        %   Skapad av Victor B�ckman, Kandidatarbete SSYX02-15-38 v�ren 2015.
        
        m=500; % Skoterns massa
        
        if riktning == 1
            F = m*a*cos(alpha);
        elseif riktning == 2 || riktning == 3
            F = m*a*sin(alpha);
        end
        
    end

