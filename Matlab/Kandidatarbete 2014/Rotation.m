function R = Rotation(theta)
        %Rotation Roterar 2D-objektet matematiskt positivt i radianer.
        %   Skapad av Victor B�ckman, Kandidatarbete SSYX02-15-38 v�ren 2015.
        
        theta=-theta;
        
        R=[cos(theta) -sin(theta);sin(theta) cos(theta)];
        
    end

