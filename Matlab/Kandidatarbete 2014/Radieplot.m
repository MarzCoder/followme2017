function Radieplot(Bana, R)
%Radieplot Plottar godk�nd radie runt waypoints.
%   Skapad av Victor B�ckman, Kandidatarbete SSYX02-15-38 v�ren 2015.


t=linspace(0,2*pi,50);
cirk_x=cos(t);
cirk_y=sin(t);


[Antal_WP, ~] = size(Bana);
for k=1:Antal_WP
    x0=Bana(k,1);
    y0=Bana(k,2);
    
    x =  R.*cirk_x + x0;
    y =  R.*cirk_y + y0;
    
    plot(x,y,'m')
end
end

