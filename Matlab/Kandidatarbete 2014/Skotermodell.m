function [P_global, V, Theta] = Skotermodell(P0, V0, Theta, dt, a, alpha)
        %Skotermodell Ber�knar hur skotern kommer att bete sig lokalt.
        %   Datan transformeras sen till ett globalt koordinaysystem beroende
        %   p� rymdvinkeln (theta) och returneras.
        %   Skapad av Victor B�ckman, Kandidatarbete SSYX02-15-38 v�ren 2015.
        
        %%% Initiering %%%
        Vf=[];
        Vd=[];
        Omega=[];
        P_global=[];
        V=[];
        
        %%% Konstanter %%%
        l = 4;                  %m    ENDAST ANTAGANDE
        b = 1.5;                %m    ENDAST ANTAGANDE
        r = sqrt(4*b^2 + l^2) *(1/4);
        m = 500;                %kg   ENDAST ANTAGANDE
        Iz=(m/12)*(l^2 + b^2);  % Antar att skotern �r en rektangel med massan m
        a_max=20;
        alpha_max=30*pi/180;
        k=1;
        
        Vf(k)=V0(1);
        Vd(k)=V0(2);
        Omega(k)=V0(3);
        P_global(k,:)=P0(1,:);
        Theta(k)=Theta;
        
        %%% Kollar att v�rden �r ok %%%
        if alpha>alpha_max
            alpha=30*pi/180;
            %disp(['G�r ej att sv�nga mer �n ', num2str(alpha_max*180/pi), 'grader'])
        elseif alpha<-alpha_max
            alpha=-30*pi/180;
            %disp(['G�r ej att sv�nga mer �n ', num2str(-alpha_max*180/pi), 'grader'])
        elseif a>a_max
            a=a_max;
            %disp(['G�r ej att accelerera mer �n', num2str(a_max), 'm/s^2'])
        elseif a<0
            a=0;
            %disp('G�r ej att backa')
        end
        
        %%% Ber�kningar av nya hastigheter %%%
        % Roterar %
        M_jet = Fjet(3,a,alpha)*l/2;
        V_theta = Omega(k)*r;
        M_motstand = Motstand(3,V_theta(k))*(l/4);
        
        if alpha == 0
            Omega(k+1)=0;
        else
            Omega(k+1) = (dt/Iz)*(M_jet - M_motstand) + Omega(k);
        end
        
        % Fram�t
        Vf(k+1) = (dt/m)*(Fjet(1,a,alpha) - Motstand(1,Vf(k))) + Vf(k);
        
        % Driver
        Vd(k+1) = (dt/m)*(Fjet(2,a,alpha) - Motstand(2,Vd(k))) + Vd(k);
        
        % Hastighetsvektor
        V(k+1,:) = [Vf(k+1) Vd(k+1) Omega(k+1)];
        
        %%% Positionvektorer %%%
        Theta(k+1) = Omega(k+1)*dt + Theta(k);
        
        Transform=[cos(Theta(k+1)) sin(Theta(k+1)); cos(Theta(k+1)) -sin(Theta(k+1))];
        
        P_global(k+1,:) = V(k+1,[1 2])*dt*Transform + P_global(k,:);  % Transformerar senaste str�ckbiten
        
        %%% Returnera %%%
        P_global=P_global(k+1,:);
        Theta=Theta(k+1);
        V=V(k+1,:);
        
    end

