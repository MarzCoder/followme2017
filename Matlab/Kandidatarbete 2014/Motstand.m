function D = Motstand(riktning, v )
        %Motstand Returnerar kraften fr�n vattenmotst�ndet f�r olika riktningar.
        %   Konstanter beror p� b�tens hastighet.
        %   Skapad av Victor B�ckman, Kandidatarbete SSYX02-15-38 v�ren 2015.
        
        roh = 1025;    % kg/m^3, vattens densitet
        
        if v>0 && v<6      % Skotern har inte planat
            Cd_d = 1;       % Sidan �r som en fyrkant mot vattnet
            Cd_f = 0.5;     % Framsidan �r som en kon
            
            A_d = 1;        % Sidans area ligger djupt i vattnet
            A_f = 0.5;      % Framsidans area ligger djupt i vattnet
            
        else                % Skotern har planat
            Cd_d = 0.5;     % Sidan �r som en kon
            Cd_f = 0.1;    % Framsidan �r v�ldigt str�mlinjeformad
            
            A_d = 0.5;        % Sidans area ligger inte djupt i vattnet
            A_f = 0.3;      % Framsidans area ligger inte djupt i vattnet
        end
        Cd_theta = Cd_d;
        
        if riktning == 1
            D = (1/2)*Cd_f*roh*A_f *v^2;
        elseif riktning == 2
            D = (1/2)*Cd_d*roh*A_d *v^2;
        elseif riktning == 3
            D = 2*(1/2)*Cd_theta*roh*A_d/2 *v^2;
        end
        
        if v<0
            D=-D;
        end
        
    end
