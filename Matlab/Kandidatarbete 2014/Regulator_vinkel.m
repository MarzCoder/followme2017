 function alpha = Regulator_vinkel(alpha0, err_vink0, err_vink, dt, V)
        %Regulator_vinkel PID-regulator f�r jetvinkeln.
        %   Skapad av Victor B�ckman, Kandidatarbete SSYX02-15-38 v�ren 2015.
        
        % Korrigering av reg.param beroende av hastighet %
            V_skoter=sqrt(abs(V(1)^2 + V(2)^2));
            if V_skoter < 5
                K_vink=[0.5 0.5 0.5];               % [Kp Ki Kd]
            elseif V_skoter >= 5 && V_skoter < 10
                K_vink=[0.2 0.3 0.5];
            else
                K_vink=[0.1 0.2 0.6];
            end
            
        vinkelhast_max=60*pi/180;   % G�r ej att sv�nga snabbare �n fullt utslag p� 1 sekund.
        
        int = (err_vink0 + err_vink)/2 * dt;  % Trapetsmetoden
        deriv= (err_vink - err_vink0)/dt;
        
        alpha = K_vink(1)*err_vink + K_vink(2)*int + K_vink(3)*deriv;
        
        vinkelhast=(alpha-alpha0)/dt;
        
        if vinkelhast > vinkelhast_max
            alpha =vinkelhast_max*dt + alpha0;
            %disp(['G�r ej att sv�nga snabbare �n', num2str(vinkelhast_max*180/pi), 'rad/sek'])
        elseif vinkelhast < -vinkelhast_max
            alpha =-vinkelhast_max*dt + alpha0;
            %disp(['G�r ej att sv�nga snabbare �n', num2str(-vinkelhast_max*180/pi), 'rad/sek'])
        end
    end

