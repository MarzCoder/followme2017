 function Skoterplot(alpha, Theta, a, V)
        %Skoterplot Plottar skotern rymdvinkel samt hastighet, acc osv.
        %   Anv�nds under simulering f�r grafisk feedback.
        %   Skapad av Victor B�ckman, Kandidatarbete SSYX02-15-38 v�ren 2015.
        
        %Polyline av en skoter
        poly=[-2 2 4 2 -2 -2; -1 -1 0 1 1 -1]; %F�rsta raden �r x-kord och andra y-kord
        
        %Roterar skotern
        for i=1:length(poly)
            poly(:,i)=poly(:,i)'*Rotation(Theta);
        end
        
        %Plottar skotern
        hold off
        plot(poly(1,:),poly(2,:))
        axis([-5 10 -5 5])
        hold on
        
        % Plottar v�rden %
        V_tot = sqrt(abs(V(1)^2 + V(2)^2)); % Total hastighet
        
        text(5,3.5,['alpha = ', num2str(alpha*180/pi)])
        text(5,2,['theta = ', num2str(Theta*180/pi)])
        text(5,-2,['acc = ', num2str(a)])
        text(5,-3.5,['v = ', num2str(V_tot)])
        hold on
    end

