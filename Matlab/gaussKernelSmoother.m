function [ y ] = gaussKernelSmoother(t, x, sigma )

[m, n] = size(x); 

y = zeros(m,n); 

for i =1:m
   
    a = -( t(i)-t(:) ).^2./(2*sigma^2);
    
    w = exp(a)*ones(1, n); 
    
    
    y(i,:) = sum(w.*x)./sum(w); 
    
end


end

