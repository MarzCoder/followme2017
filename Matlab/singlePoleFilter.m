function [ y ] = singlePoleFilter( x, Tf, Ts )
%% Do single pole filtering on x, based on 
%  filter constant Tf and sampling time Ts 

N = length(x); 

% The input x to be a matrix with column vectors 
y      = zeros(size(x)); 

y(1,:) = x(1,:); 

alpha = Ts /(Tf+Ts); 

for i=2:N
    y(i,:) = y(i-1,:)*(1-alpha)+x(i,:)*alpha;  
end 





end

